import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Login } from '../components/Login';
import { Dashboard } from '../components/Dashboard';
import { ResetPassword } from '../components/ResetPassword';
import { AccountActivation } from '../components/AccountActivation';

import { Authenticate, NoAuthenticate, OrganizationVerification } from '../components/_decorators/';

const immerseOnlineLoginUrl = process.env.REACT_APP_IMMERSE_ONLINE_LOGIN_URL || "https://www.immerse.online";

export default [
  <Route key="login" exact path="/" component={OrganizationVerification(NoAuthenticate(Login))} />,
  <Route key="resetPassword" path="/resetPassword/:accessToken" component={OrganizationVerification(NoAuthenticate(ResetPassword))}/>,
  <Route key="accountActivation" path="/activate/:accessToken" component={OrganizationVerification(NoAuthenticate(AccountActivation))}/>,
  <Route key="home" path="/dashboard" component={OrganizationVerification(Authenticate(Dashboard))}/>,
  <Route key="immerseOnlineRedirect" exact path="/redirect" render={() => {window.location = `${immerseOnlineLoginUrl}`; return null;}}/>,
  <Route key="redirect" render={() => <Redirect to="/"/>}/>,
];
