import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { StudentClassPurchase } from '../components/StudentClassPurchase';
import { StudentMyClasses } from '../components/StudentMyClasses';
import { StudentMyProfile } from '../components/StudentMyProfile';

export default [
  <Route
    key="My Courses"
    exact path="/dashboard"
    component={StudentMyClasses}
  />,
  <Route
    key="Book a Course"
    path="/dashboard/purchase"
    component={StudentClassPurchase}
  />,
  <Route
    key="My Profile"
    path="/dashboard/profile"
    component={StudentMyProfile}
  />, 
  <Route
    key="Redirect"
    render={() => (
      <Redirect to="/dashboard"/>
    )}
  />
];
