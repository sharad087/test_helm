import {
  ACTIVATE_ACCOUNT_START,
  ACTIVATE_ACCOUNT_SUCCESS,
  ACTIVATE_ACCOUNT_ERROR,
} from './types';

//ACTIVATE ACCOUNT
const activateAccount = (name, password, token) => {
  return {
    type: ACTIVATE_ACCOUNT_START,
    name,
    password,
    token,
  }
}

const activateAccountSuccess = payload => {
  return {
    type: ACTIVATE_ACCOUNT_SUCCESS,
    payload
  }
}

const activateAccountError = err => {
  return {
    type: ACTIVATE_ACCOUNT_ERROR,
    err
  }
}

// Actions to dispatch to React components
export { activateAccount };

// Actions that are called within reducers.js file within Redux-Loop
export { activateAccountSuccess, activateAccountError }