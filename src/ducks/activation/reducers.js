import { loop, Cmd } from "redux-loop";
import { setCookie } from "../../utils";
import * as api from "./api";

import {
  ACTIVATE_ACCOUNT_START,
  ACTIVATE_ACCOUNT_SUCCESS,
  ACTIVATE_ACCOUNT_ERROR
} from "./types";

import { activateAccountSuccess, activateAccountError } from "./actions";

const initialState = {
  busy: false,
  hasActivatedAccount: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIVATE_ACCOUNT_START:
      return loop(
        {
          ...state,
          busy: true,
          hasActivatedAccount: false
        },
        Cmd.run(api.activateAccount, {
          successActionCreator: activateAccountSuccess,
          failActionCreator: activateAccountError,
          args: [action.name, action.password, action.token]
        })
      );

    case ACTIVATE_ACCOUNT_SUCCESS:
      setCookie("refreshToken", action.payload.data.refreshToken);
      
      return {
        ...state,
        busy: false,
        hasActivatedAccount: true,
        error: null
      };

    case ACTIVATE_ACCOUNT_ERROR:
      return {
        ...state,
        busy: false,
        hasActivatedAccount: false,
        error: action.err.response.data.message,
      };

    default:
      return state;
  }
};

export default reducer;
