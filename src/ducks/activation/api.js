import axios from "axios";

const activateAccount = (name, password, token) => {
  return axios.patch(
    `/public/auths`,
    {
      name,
      password,
      token
    },
  );
};

export { activateAccount };
