import { createSelector } from 'reselect';

const activationBusySelector = state => state.activation.busy;
export const selectActivationBusy = createSelector(activationBusySelector, busy => busy);

const activationHasActivatedAccountSelector = state => state.activation.hasActivatedAccount;
export const selectActivationHasActivatedAccount = createSelector(activationHasActivatedAccountSelector, hasActivatedAccount => hasActivatedAccount);

const activationErrorSelector = state => state.activation.error;
export const selectActivationError = createSelector(activationErrorSelector, error => error);