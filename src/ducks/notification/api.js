import axios from 'axios';
import { readCookie } from "../../utils/";

const getNotification = query => {
  return axios.get(`/private/notifications`, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    },
    params: {
      ...query
    }
  });
}

const readNotification = id => {
  return axios.patch(`/private/notifications/${id}`, {}, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    }
  })
}

export { getNotification, readNotification };