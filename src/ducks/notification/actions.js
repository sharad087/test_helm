import {
  GET_NOTIFICATION_START,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_ERROR,
  READ_NOTIFICATION_START,
  READ_NOTIFICATION_SUCCESS,
  READ_NOTIFICATION_ERROR
} from "./types";

//GET NOTIFICATION
const getNotification = query => {
  return {
    type: GET_NOTIFICATION_START,
    query
  };
};

const getNotificationSuccess = payload => {
  return {
    type: GET_NOTIFICATION_SUCCESS,
    payload
  };
};

const getNotificationError = err => {
  return {
    type: GET_NOTIFICATION_ERROR,
    err
  };
};

//READ NOTIFICATION
const readNotification = id => {
  return {
    type: READ_NOTIFICATION_START,
    id
  };
};

const readNotificationSuccess = payload => {
  return {
    type: READ_NOTIFICATION_SUCCESS,
    payload
  };
};

const readNotificationError = err => {
  return {
    type: READ_NOTIFICATION_ERROR,
    err
  };
};

// Actions to dispatch to React components
export { getNotification, readNotification };

// Actions that are called within reducers.js file within Redux-Loop
export {
  getNotificationSuccess,
  getNotificationError,
  readNotificationSuccess,
  readNotificationError
};
