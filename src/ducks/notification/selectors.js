import { createSelector } from "reselect";

const notificationBusySelector = state => state.notification.busy;
export const selectNotificationBusy = createSelector(
  notificationBusySelector,
  busy => busy
);

const notificationLoadedSelector = state => state.notification.loaded;
export const selectNotificationLoaded = createSelector(
  notificationLoadedSelector,
  loaded => loaded
);

const notificationErrorSelector = state => state.notification.error;
export const selectNotificationError = createSelector(
  notificationErrorSelector,
  error => error
);

const notificationDataSelector = state => state.notification.data;
export const selectNotificationData = createSelector(
  notificationDataSelector,
  data => data
);
