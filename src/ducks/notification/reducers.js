import { loop, Cmd } from "redux-loop";

import * as api from "./api";

import {
  GET_NOTIFICATION_START,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_ERROR,
  READ_NOTIFICATION_START,
  READ_NOTIFICATION_SUCCESS,
  READ_NOTIFICATION_ERROR
} from "./types";

import {
  getNotificationSuccess,
  getNotificationError,
  readNotificationSuccess,
  readNotificationError
} from "./actions";

const initialState = {
  busy: false,
  loaded: false,
  error: null,
  busyReadingNotification: false,
  hasReadNotification: false,
  readNotificationError: null,
  data: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATION_START:
      return loop(
        {
          ...state,
          busy: true,
          loaded: false
        },
        Cmd.run(api.getNotification, {
          successActionCreator: getNotificationSuccess,
          failActionCreator: getNotificationError,
          args: [action.query]
        })
      );

    case GET_NOTIFICATION_SUCCESS:
      return {
        ...state,
        busy: false,
        loaded: true,
        error: null,
        data: action.payload.data
      };

    case GET_NOTIFICATION_ERROR:
      return {
        ...state,
        busy: false,
        loaded: false,
        error: action.err.response.data.message
      };

    //READ NOTIFICATION
    case READ_NOTIFICATION_START:
      return loop(
        {
          ...state,
        },
        Cmd.run(api.readNotification, {
          successActionCreator: readNotificationSuccess,
          failActionCreator: readNotificationError,
          args: [action.id]
        })
      );

    case READ_NOTIFICATION_SUCCESS:
      return {
        ...state,
        readNotificationError: null,
        data: state.data.map(notification =>
          notification._id === action.payload.data.id
            ? {...notification, read: true}
            : notification
        )
      }

    case READ_NOTIFICATION_ERROR:
      return {
        ...state,
        readNotificationError: action.err.response.data.message
      }

    default:
      return state;
  }
};

export default reducer;
