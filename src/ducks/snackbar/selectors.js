import { createSelector } from 'reselect';

const errorSelector = state => state.snackbar.error;
const successSelector = state => state.snackbar.success;

export const selectError = createSelector(
  errorSelector,
  error => error
);

export const selectSuccess = createSelector(
  successSelector,
  success => success
);
