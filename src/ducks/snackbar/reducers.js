import {
  SET_SUCCESS,
  SET_ERROR,
  RESET_SNACKBARS,
} from './types';

const initialState = {
  error: {
    open: false,
    status: 0,
    type: '',
    message: ''
  },
  success: {
    open: false,
    status: 0,
    type: '',
    message: ''
  }
}

const reducer = (state = initialState, action) => {
  switch(action.type){
    case SET_ERROR:
    if (!action.payload.error.status) {
      return {
        ...state,
        error: {
          open: true,
          status: 404,
          type: 'NOT FOUND',
          message: 'Internal server error'
        }
      };
    } else {
      return {
        ...state,
        error: {
          open: true,
          status: action.payload.error.status,
          type: action.payload.error.type,
          message: action.payload.error.message
        }
      };
    }

    case SET_SUCCESS:
      return {
        ...state,
        success: {
          open: true,
          status: action.payload.success.status,
          type: action.payload.success.type,
          message: action.payload.success.message
        }
      }

    case RESET_SNACKBARS:
      return initialState;

    default:
      return state;
  }
}

export default reducer;