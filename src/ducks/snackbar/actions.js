import {
  SET_ERROR,
  SET_SUCCESS,
  RESET_SNACKBARS,
} from './types';

const setError = error => {
  return {
    type: SET_ERROR,
    payload: { error }
  }
}

const setSuccess = success => {
  return {
    type: SET_SUCCESS,
    payload: { success },
  }
}

const resetSnackbars = () => {
  return {
    type: RESET_SNACKBARS,
  }
}

// Actions to dispatch to React components
export { setError, setSuccess, resetSnackbars };

// Actions that are called within reducers.js file within Redux-Loop
export {};
