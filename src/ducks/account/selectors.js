import { createSelector } from 'reselect';

const accountErrorSelector = state => state.account.error;
export const selectAccountError = createSelector(accountErrorSelector, error => error);

const accountIsLoadingProfileSelector = state => state.account.isLoadingProfile;
export const selectAccountIsLoadingProfile = createSelector(accountIsLoadingProfileSelector, isLoadingProfile => isLoadingProfile);

const accountHasLoadedProfile = state => state.account.hasLoadedProfile;
export const selectAccountHasLoadedProfile = createSelector(accountHasLoadedProfile, hasLoadedProfile => hasLoadedProfile);

const accountProfileSelector = state => state.account.profile;
export const selectAccountProfile = createSelector(accountProfileSelector, profile => profile);

//UPDATE PROFILE
const accountIsUpdatingProfile = state => state.account.isUpdatingProfile;
export const selectAccountIsUpdatingProfile = createSelector(accountIsUpdatingProfile, isUpdatingProfile => isUpdatingProfile);

const accountHasUpdatedProfile = state => state.account.hasUpdatedProfile;
export const selectAccountHasUpdatedProfile = createSelector(accountHasUpdatedProfile, hasUpdatedProfile => hasUpdatedProfile);

const accountUpdateProfileError = state => state.account.accountUpdateProfileError;
export const selectAccountUpdateProfileError = createSelector(accountUpdateProfileError, updateProfileError => updateProfileError);

const accountHasUpdatedProfileImage = state => state.account.hasUpdatedProfileImage;
export const selectAccountHasUpdatedProfileImage = createSelector(accountHasUpdatedProfileImage, hasUpdatedProfileImage => hasUpdatedProfileImage);