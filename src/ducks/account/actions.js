import {
  CREATE_ACCOUNT_START,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_ERROR,
  GET_PROFILE_START,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_ERROR,
  UPDATE_PROFILE_START,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_ERROR,
  UPLOAD_PROFILE_IMAGE_START,
  UPLOAD_PROFILE_IMAGE_SUCCESS,
  UPLOAD_PROFILE_IMAGE_ERROR,
} from "./types";

//CREATE ACCOUNT
const createAccount = (email, password, fullName) => {
  return {
    type: CREATE_ACCOUNT_START,
    payload: {
      email,
      password,
      fullName
    }
  };
};

const createAccountSuccess = () => {
  return {
    type: CREATE_ACCOUNT_SUCCESS
  };
};

const createAccountError = err => {
  return {
    type: CREATE_ACCOUNT_ERROR,
    err
  };
};

//GET PROFILE
const getProfile = accessToken => {
  return {
    type: GET_PROFILE_START,
    accessToken
  };
};

const getProfileSuccess = payload => {
  return {
    type: GET_PROFILE_SUCCESS,
    payload
  };
};

const getProfileError = err => {
  return {
    type: GET_PROFILE_ERROR,
    err
  };
};

//UPDATE PROFILE
const updateProfile = updateProps => {
  return {
    type: UPDATE_PROFILE_START,
    updateProps
  };
};

const updateProfileSuccess = payload => {
  return {
    type: UPDATE_PROFILE_SUCCESS,
    payload,
  };
};

const updateProfileError = err => {
  return {
    type: UPDATE_PROFILE_ERROR,
    err
  }
}

//UPLOAD PROFILE IMAGE
const uploadProfileImage = image => {
  return {
    type: UPLOAD_PROFILE_IMAGE_START,
    profileImage: image
  }
}

const uploadProfileImageSuccess = () => {
  return {
    type: UPLOAD_PROFILE_IMAGE_SUCCESS,
  }
}

const uploadProfileImageError = err => {
  return {
    type: UPLOAD_PROFILE_IMAGE_ERROR,
    err
  }
}

// Actions to dispatch to React components
export { createAccount, getProfile, updateProfile, uploadProfileImage };

// Actions that are called within reducers.js file within Redux-Loop
export {
  createAccountSuccess,
  createAccountError,
  getProfileSuccess,
  getProfileError,
  updateProfileSuccess,
  updateProfileError,
  uploadProfileImageSuccess,
  uploadProfileImageError,
};
