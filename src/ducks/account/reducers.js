import { loop, Cmd } from "redux-loop";
import { errorMessage } from "../../data";

import {
  CREATE_ACCOUNT_START,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_ERROR,
  GET_PROFILE_START,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_ERROR,
  UPDATE_PROFILE_START,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_ERROR,
  UPLOAD_PROFILE_IMAGE_START,
  UPLOAD_PROFILE_IMAGE_SUCCESS,
  UPLOAD_PROFILE_IMAGE_ERROR
} from "./types";

import {
  createAccountSuccess,
  createAccountError,
  getProfileSuccess,
  getProfileError,
  updateProfileSuccess,
  updateProfileError,
  uploadProfileImageSuccess,
  uploadProfileImageError
} from "./actions";

import { setError } from "../snackbar/actions";

import * as api from "./api";

const initialState = {
  busy: false,
  error: null,
  isLoadingProfile: false,
  hasLoadedProfile: false,
  isUpdatingProfile: false,
  hasUpdatedProfile: false,
  hasUpdatedProfileImage: false,
  updateProfileError: null,
  uploadProfileImageError: null,
  profile: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ACCOUNT_START:
      const { email, password, fullName } = action.payload;
      return loop(
        {
          ...state,
          busy: true
        },
        Cmd.run(api.createAccount, {
          successActionCreator: createAccountSuccess,
          failActionCreator: createAccountError,
          args: [email, password, fullName]
        })
      );

    case CREATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        busy: false,
        error: null
      };

    case CREATE_ACCOUNT_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response.data.message
      };

    case GET_PROFILE_START:
      return loop(
        {
          ...state,
          isLoadingProfile: true,
          hasLoadedProfile: false
        },
        Cmd.run(api.getProfile, {
          successActionCreator: getProfileSuccess,
          failActionCreator: getProfileError,
          args: [action.accessToken]
        })
      );

    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        error: null,
        isLoadingProfile: false,
        hasLoadedProfile: true,
        profile: action.payload.data
      };
    case GET_PROFILE_ERROR:
      return {
        ...state,
        isLoadingProfile: false,
        hasLoadedProfile: false,
        error: action.err.response.data.message
      };

    case UPDATE_PROFILE_START:
      return loop(
        {
          ...state,
          isUpdatingProfile: true,
          hasUpdatedProfile: false
        },
        Cmd.run(api.updateProfile, {
          successActionCreator: updateProfileSuccess,
          failActionCreator: updateProfileError,
          args: [action.updateProps]
        })
      );

    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        isUpdatingProfile: false,
        hasUpdatedProfile: true,
        error: null
      };

    case UPDATE_PROFILE_ERROR:
      return loop(
        {
          ...state,
          isUpdatingProfile: false,
          error: action.err.response.data.message
        },
        Cmd.action(setError(errorMessage["updateProfile"]))
      );

    case UPLOAD_PROFILE_IMAGE_START:
      return loop(
        {
          ...state,
          isUpdatingProfile: true,
          hasUpdatedProfileImage: false
        },
        Cmd.run(api.uploadProfileImage, {
          successActionCreator: uploadProfileImageSuccess,
          failActionCreator: uploadProfileImageError,
          args: [action.profileImage]
        })
      );

    case UPLOAD_PROFILE_IMAGE_SUCCESS:
      return {
        ...state,
        isUpdatingProfile: false,
        hasUpdatedProfileImage: true,
        uploadProfileImageError: null
      };

    case UPLOAD_PROFILE_IMAGE_ERROR:
      return {
        ...state,
        isUpdatingProfile: false,
        hasUpdatedProfileImage: false,
        uploadProfileImageError: action.err.response.data.message
      };

    default:
      return state;
  }
};

export default reducer;
