import axios from "axios";
import FormData from "form-data";
import { readCookie } from "../../utils/";

const apiRoute = process.env.REACT_APP_API_URL || "";

const createAccount = (email, password, fullName) => {
  return axios.post(`${apiRoute}/user/student`, {
    email,
    password,
    name: fullName
  });
};

const getProfile = accessToken => {
  const authAccessToken = accessToken || readCookie("accessToken");

  return axios.get(`/private/self`, {
    headers: {
      Authorization: `Bearer ${authAccessToken}`
    }
  });
};

const updateProfile = updateProps => {
  return axios.put(
    `/private/self`,
    {
      ...updateProps
    },
    {
      headers: {
        Authorization: `Bearer ${readCookie("accessToken")}`
      }
    }
  );
};

const uploadProfileImage = image => {
  let imageData = new FormData();
  imageData.append("profileImage", image);

  return axios.put(`/private/self/profileImage`, imageData, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    }
  });
};

const sendResetPassword = email => {
  return axios.post(`/public/auths/passwordReset`, {
    email,
    role: 'student',
  });
};

export {
  createAccount,
  getProfile,
  updateProfile,
  uploadProfileImage,
  sendResetPassword,
};
