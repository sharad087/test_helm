import axios from "axios";

const updatePassword = (password, token) => {
  return axios.put(
    `/public/auths/password`,
    {
      password,
      token,
    }
  );
};

export { updatePassword };