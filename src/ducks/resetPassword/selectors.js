import { createSelector } from 'reselect';

//RESET PASSWORD
const resetPasswordBusy = state => state.resetPassword.busy;
export const selectResetPasswordBusy = createSelector(resetPasswordBusy, busy => busy);

const resetPasswordReset = state => state.resetPassword.reset;
export const selectResetPasswordReset = createSelector(resetPasswordReset, reset => reset);

const resetPasswordError = state => state.resetPassword.error;
export const selectResetPasswordError = createSelector(resetPasswordError, error => error);