import {
  RESET_PASSWORD_START,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR
} from "./types";

//RESET PASSWORD
const resetPassword = (token, password) => {
  return {
    type: RESET_PASSWORD_START,
    password,
    token,
  }
}

const resetPasswordSuccess = payload => {
  return {
    type: RESET_PASSWORD_SUCCESS,
    payload,
  }
}

const resetPasswordError = err => {
  return {
    type: RESET_PASSWORD_ERROR,
    err,
  }
}

// Actions to dispatch to React components
export { resetPassword }

// Actions that are called within reducers.js file within Redux-Loop
export { resetPasswordSuccess, resetPasswordError };