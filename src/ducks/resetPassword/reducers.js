import { loop, Cmd } from "redux-loop";
import * as api from "./api";

import {
  RESET_PASSWORD_START,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR
} from "./types";

import { resetPasswordSuccess, resetPasswordError } from "./actions";

const initialState = {
  busy: false,
  reset: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_PASSWORD_START:
      return loop(
        {
          ...state,
          busy: true,
          hasResetPassword: false
        },
        Cmd.run(api.updatePassword, {
          successActionCreator: resetPasswordSuccess,
          failActionCreator: resetPasswordError,
          args: [action.token, action.password]
        })
      );

    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        busy: false,
        reset: true,
        error: null
      };

    case RESET_PASSWORD_ERROR:
      return {
        ...state,
        busy: false,
        reset: false,
        error: action.err.response.data.message
      };

    default:
      return state;
  }
};

export default reducer;