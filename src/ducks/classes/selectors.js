import { createSelector } from "reselect";

const classesBusySelector = state => state.classes.busy;
const classesErrorSelector = state => state.classes.error;
const classesLoadedSelector = state => state.classes.loaded;
const classesIsFuzzySearchSelector = state => state.classes.isFuzzySearch;
const classesMyClassesSkipSelector = state => state.classes.myClassesSkip;
const classesMyClassesNoMoreSelector = state => state.classes.myClassesNoMore;
const classesMyClassesQuerySelector = state => state.classes.myClassesQuery;
const classesIsMyClassesInitialized = state => state.classes.isMyClassesInitialized;
const classesStoreClassesSkipSelector = state => state.classes.storeClassesSkip;
const classesStoreClassesNoMoreSelector = state => state.classes.storeClassesNoMore;
const classesStoreClassesQuerySelector = state => state.classes.storeClassesQuery;
const classesIsStoreClassesInitialized = state => state.classes.isStoreClassesInitialized;
const classesStoreClassesSelector = state => state.classes.data.storeClasses;
const classesMyClassesSelector = state => state.classes.data.myClasses;

const classesIsRequestingEnrollment = state => state.classes.isRequestingEnrollment;
const classesRequestEnrollmentError = state => state.classes.requestEnrollmentError;

const classesLobbyUrl = state => state.classes.lobbyUrl;
const classesGotLobbyUrl = state => state.classes.gotLobbyUrl;
const classesBusyLobbyUrl = state => state.classes.busyLobbyUrl;

export const selectClassesBusy = createSelector(
  classesBusySelector,
  busy => busy,
);

export const selectClassesError = createSelector(
  classesErrorSelector,
  error => error
);

export const selectClassesLoaded = createSelector(
  classesLoadedSelector,
  loaded => loaded,
)

export const selectClassesIsFuzzySeach = createSelector(
  classesIsFuzzySearchSelector,
  isFuzzySearch => isFuzzySearch
)

export const selectClassesMyClassesSkip = createSelector(
  classesMyClassesSkipSelector,
  myClassesSkip => myClassesSkip
)

export const selectClassesMyClassesNoMore = createSelector(
  classesMyClassesNoMoreSelector,
  myClassesNoMore => myClassesNoMore
)

export const selectClassesMyClassesQuery = createSelector(
  classesMyClassesQuerySelector,
  myClassesQuery => myClassesQuery
)

export const selectClassesIsMyClassesInitialzied = createSelector(
  classesIsMyClassesInitialized,
  isMyClassesInitialized => isMyClassesInitialized
)

export const selectClassesStoreClassesSkip = createSelector(
  classesStoreClassesSkipSelector,
  storeClassesSkip => storeClassesSkip
)

export const selectClassesStoreClassesNoMore = createSelector(
  classesStoreClassesNoMoreSelector,
  storeClassesNoMore => storeClassesNoMore
)

export const selectClassesStoreClassesQuery = createSelector(
  classesStoreClassesQuerySelector,
  storeClassesQuery => storeClassesQuery
)

export const selectClassesIsStoreClassesInitialized = createSelector(
  classesIsStoreClassesInitialized,
  isStoreClassesInitialized => isStoreClassesInitialized
)

export const selectClassesStoreClasses = createSelector(
  classesStoreClassesSelector,
  storeClasses => storeClasses
);

export const selectClassesMyClasses = createSelector(
  classesMyClassesSelector,
  myClasses => myClasses
)

//Request Enrollment
export const selectClassesIsRequestingEnrollment = createSelector(
  classesIsRequestingEnrollment,
  isRequestingEnrollment => isRequestingEnrollment
)

export const selectClassesRequestEnrollmentError = createSelector(
  classesRequestEnrollmentError,
  requestEnrollmentError => requestEnrollmentError,
)

//Get lobby Url
export const selectClassesLobbyUrl = createSelector(
  classesLobbyUrl,
  lobbyUrl => lobbyUrl,
)

export const selectClassesGotLobbyUrl = createSelector(
  classesGotLobbyUrl,
  gotLobbyUrl => gotLobbyUrl,
)

export const selectClassesBusyLobbyUrl = createSelector(
  classesBusyLobbyUrl,
  busyLobbyUrl => busyLobbyUrl,
)