import {
  GET_MY_CLASSES_START,
  GET_MY_CLASSES_SUCCESS,
  GET_MY_CLASSES_ERROR,
  GET_STORE_CLASSES_START,
  GET_STORE_CLASSES_SUCCESS,
  GET_STORE_CLASSES_ERROR,
  FUZZY_SEARCH_STORE_CLASSES_START,
  FUZZY_SEARCH_STORE_CLASSES_SUCCESS,
  FUZZY_SEARCH_STORE_CLASSES_ERROR,
  REQUEST_ENROLLMENT_START,
  REQUEST_ENROLLMENT_SUCCESS,
  REQUEST_ENROLLMENT_ERROR,
  GET_LOBBY_URL_START,
  GET_LOBBY_URL_SUCCESS,
  GET_LOBBY_URL_ERROR,
} from './types';

//GET MY CLASSES
const getMyClasses = (limit=6, skip=0, query) => {
  return {
    type: GET_MY_CLASSES_START,
    limit,
    skip,
    query,
  }
}

const getMyClassesSuccess = payload => {
  return {
    type: GET_MY_CLASSES_SUCCESS,
    payload,
  }
}

const getMyClassesError = err => {
  return {
    type: GET_MY_CLASSES_ERROR,
    err,
  }
}

//GET STORE CLASSES
const getStoreClasses = (limit=15, skip=0, query) => {
  return {
    type: GET_STORE_CLASSES_START,
    limit,
    skip,
    query,
  }
}

const getStoreClassesSuccess = payload => {
  return {
    type: GET_STORE_CLASSES_SUCCESS,
    payload,
  }
}

const getStoreClassesError = err => {
  return {
    type: GET_STORE_CLASSES_ERROR,
    err,
  }
}

//FUZZY SEARCH STORE CLASSES
const fuzzySearchStoreClasses = (limit=15, skip=0, query) => {
  return {
    type: FUZZY_SEARCH_STORE_CLASSES_START,
    limit,
    skip,
    query,
  }
}

const fuzzySearchStoreClassesSuccess = payload => {
  return {
    type: FUZZY_SEARCH_STORE_CLASSES_SUCCESS,
    payload,
  }
}

const fuzzySearchStoreClassesError = err => {
  return {
    type: FUZZY_SEARCH_STORE_CLASSES_ERROR,
    err,
  }
}

//REQUEST ENROLLMENT
const requestEnrollment = classId => {
  return {
    type: REQUEST_ENROLLMENT_START,
    classId,
  }
}

const requestEnrollmentSuccess = () => {
  return {
    type: REQUEST_ENROLLMENT_SUCCESS,
  }
}

const requestEnrollmentError = err => {
  return {
    type: REQUEST_ENROLLMENT_ERROR,
    err,
  }
}

//GET LOBBY URL
const getLobbyUrl = courseId => {
  return {
    type: GET_LOBBY_URL_START,
    courseId,
  }
}

const getLobbyUrlSuccess = payload => {
  return {
    type: GET_LOBBY_URL_SUCCESS,
    payload,
  }
}

const getLobbyUrlError = err => {
  return {
    type: GET_LOBBY_URL_ERROR,
    err,
  }
}

// Actions to dispatch to React components
export {
  getMyClasses,
  getStoreClasses,
  fuzzySearchStoreClasses,
  requestEnrollment,
  getLobbyUrl
};

// Actions that are called within reducers.js file within Redux-Loop
export {
  getMyClassesSuccess,
  getMyClassesError,
  getStoreClassesSuccess,
  getStoreClassesError,
  fuzzySearchStoreClassesSuccess,
  fuzzySearchStoreClassesError,
  requestEnrollmentSuccess,
  requestEnrollmentError,
  getLobbyUrlSuccess,
  getLobbyUrlError,
};

