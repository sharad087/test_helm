import axios from "axios";
import { readCookie } from "../../utils/";

const getStoreClasses = (limit = 15, skip = 0, query = {}) => {
  return axios.get(`/private/courses`, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    },
    params: {
      limit: limit,
      skip: skip,
      fuzzy: false,
      ...query
    }
  });
};

const fuzzySearchStoreClasses = (limit = 15, skip = 0, query = {}) => {
  return axios.get(`/private/courses`, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    },
    params: {
      limit: limit,
      skip: skip,
      fuzzy: true,
      ...query
    }
  });
};

const getMyClasses = (limit = 6, skip = 0, query = {}) => {
  return axios.get(`/private/courses`, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    },
    params: {
      limit: limit,
      skip: skip,
      ...query
    }
  });
};

const requestEnrollment = courseId => {
  return axios.post(
    `/private/enrollments/requests`,
    {
      courseId,
    },
    {
      headers: {
        Authorization: `Bearer ${readCookie("accessToken")}`
      }
    }
  );
};

const getLobbyUrl = courseId => {
  return axios.get(
    `/private/courses/${courseId}/url`,
    {
      headers: {
        Authorization: `Bearer ${readCookie("accessToken")}`
      }
    }
  )
}

export {
  getStoreClasses,
  getMyClasses,
  fuzzySearchStoreClasses,
  requestEnrollment,
  getLobbyUrl,
};
