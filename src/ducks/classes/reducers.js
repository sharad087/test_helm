import { loop, Cmd } from "redux-loop";
import { successMessage, errorMessage } from "../../data";

import * as api from "./api";

import {
  getMyClassesSuccess,
  getMyClassesError,
  getStoreClassesSuccess,
  getStoreClassesError,
  fuzzySearchStoreClassesSuccess,
  fuzzySearchStoreClassesError,
  requestEnrollmentSuccess,
  requestEnrollmentError,
  getLobbyUrlSuccess,
  getLobbyUrlError
} from "./actions";

import { setError, setSuccess } from "../snackbar/actions";

import {
  GET_MY_CLASSES_START,
  GET_MY_CLASSES_SUCCESS,
  GET_MY_CLASSES_ERROR,
  GET_STORE_CLASSES_START,
  GET_STORE_CLASSES_SUCCESS,
  GET_STORE_CLASSES_ERROR,
  FUZZY_SEARCH_STORE_CLASSES_START,
  FUZZY_SEARCH_STORE_CLASSES_SUCCESS,
  FUZZY_SEARCH_STORE_CLASSES_ERROR,
  REQUEST_ENROLLMENT_START,
  REQUEST_ENROLLMENT_SUCCESS,
  REQUEST_ENROLLMENT_ERROR,
  GET_LOBBY_URL_START,
  GET_LOBBY_URL_ERROR,
  GET_LOBBY_URL_SUCCESS
} from "./types";

const initialState = {
  busy: false,
  error: null,
  loaded: false,
  isFuzzySearch: false,
  isMyClassesInitialized: false,
  myClassesSkip: 0,
  myClassesLimit: 0,
  myClassesNoMore: false,
  myClassesQuery: {},
  storeClassesQuery: {},
  storeClassesSkip: 0,
  storeClassesLimit: 0,
  storeClassesNoMore: false,
  isStoreClassesInitialized: false,
  isRequestingEnrollment: false,
  requestEnrollmentError: null,
  data: {
    myClasses: [],
    storeClasses: []
  },
  gotLobbyUrl: false,
  busyLobbyUrl: false,
  lobbyUrl: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MY_CLASSES_START:
      return loop(
        {
          ...state,
          busy: true,
          loaded: false,
          myClassesSkip: action.skip,
          myClassesLimit: action.limit,
          myClassesQuery: { ...action.query }
        },
        Cmd.run(api.getMyClasses, {
          successActionCreator: getMyClassesSuccess,
          failActionCreator: getMyClassesError,
          args: [action.limit, action.skip, action.query]
        })
      );

    case GET_MY_CLASSES_SUCCESS:
      return {
        ...state,
        busy: false,
        error: null,
        loaded: true,
        isMyClassesInitialized: true,
        myClassesNoMore: action.payload.data.length < state.myClassesLimit,
        data: {
          ...state.data,
          myClasses:
            state.myClassesSkip === 0
              ? action.payload.data
              : [...state.data.myClasses, ...action.payload.data]
        }
      };

    case GET_MY_CLASSES_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response || true
      };

    case GET_STORE_CLASSES_START:
      return loop(
        {
          ...state,
          busy: true,
          loaded: false,
          storeClassesSkip: action.skip,
          storeClassesLimit: action.limit,
          storeClassesQuery: { ...state.storeClassesQuery, ...action.query }
        },
        Cmd.run(api.getStoreClasses, {
          successActionCreator: getStoreClassesSuccess,
          failActionCreator: getStoreClassesError,
          args: [action.limit, action.skip, action.query]
        })
      );

    case GET_STORE_CLASSES_SUCCESS:
      return {
        ...state,
        busy: false,
        error: null,
        loaded: true,
        isFuzzySearch: false,
        storeClassesNoMore:
          action.payload.data.length < state.storeClassesLimit,
        data: {
          ...state.data,
          storeClasses:
            state.storeClassesSkip === 0
              ? action.payload.data
              : [...state.data.storeClasses, ...action.payload.data]
        }
      };

    case GET_STORE_CLASSES_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response.data.message || true
      };

    case FUZZY_SEARCH_STORE_CLASSES_START:
      return loop(
        {
          ...state,
          busy: true,
          loaded: false,
          isFuzzySearch: true,
          storeClassesSkip: action.skip,
          storeClassesLimit: action.limit,
          storeClassesQuery: { ...action.query }
        },
        Cmd.run(api.fuzzySearchStoreClasses, {
          successActionCreator: fuzzySearchStoreClassesSuccess,
          failActionCreator: fuzzySearchStoreClassesError,
          args: [action.limit, action.skip, action.query]
        })
      );

    case FUZZY_SEARCH_STORE_CLASSES_SUCCESS:
      return {
        ...state,
        busy: false,
        loaded: true,
        isFuzzySearch: true,
        isStoreClassesInitialized: true,
        error: null,
        storeClassesNoMore:
          action.payload.data.length < state.storeClassesLimit,
        data: {
          ...state.data,
          storeClasses:
            state.storeClassesSkip === 0
              ? [...action.payload.data]
              : [...state.data.storeClasses, ...action.payload.data]
        }
      };

    case FUZZY_SEARCH_STORE_CLASSES_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response.data.message
      };

    case REQUEST_ENROLLMENT_START:
      return loop(
        {
          ...state,
          isRequestingEnrollment: true
        },
        Cmd.run(api.requestEnrollment, {
          successActionCreator: requestEnrollmentSuccess,
          failActionCreator: requestEnrollmentError,
          args: [action.classId]
        })
      );

    case REQUEST_ENROLLMENT_SUCCESS:
      return loop(
        {
          ...state,
          isRequestingEnrollment: false,
          requestEnrollmentError: null
        },
        Cmd.action(setSuccess(successMessage["requestEnrollment"]))
      );

    case REQUEST_ENROLLMENT_ERROR:
      return loop(
        {
          ...state,
          isRequestingEnrollment: false,
          requestEnrollmentError: action.err.response.data.message
        },
        Cmd.action(setError(errorMessage["requestEnrollment"]))
      );

    case GET_LOBBY_URL_START:
      return loop(
        { ...state, busyLobbyUrl: true, gotLobbyUrl: false },
        Cmd.run(api.getLobbyUrl, {
          successActionCreator: getLobbyUrlSuccess,
          failActionCreator: getLobbyUrlError,
          args: [action.courseId]
        })
      );
    case GET_LOBBY_URL_SUCCESS:
      return {
        ...state,
        busyLobbyUrl: false,
        gotLobbyUrl: true,
        lobbyUrl: action.payload.data.url
      };
    case GET_LOBBY_URL_ERROR:
      return loop(
        {
          ...state,
          busyLobbyUrl: false,
          gotLobbyUrl: false
        },
        Cmd.action(setError(errorMessage["getLobbyUrl"]))
      );

    default:
      return state;
  }
};

export default reducer;
