import axios from "axios";
import { readCookie } from "../../utils/";

const loginAccount = (email, password) => {
  return axios.post('/public/auths/refreshToken', {
    email,
    password,
    role: 'student',
  });
};

const logoutAccount = () => {
  return axios.delete(`/public/auths/refreshToken`, {
    headers: {
      Authorization: `Bearer ${readCookie("accessToken")}`
    }
  });
};

const refreshAccessToken = refreshToken => {
  return axios.post(`/public/auths/accessToken`, {
    refreshToken
  });
};

export { loginAccount, logoutAccount, refreshAccessToken };
