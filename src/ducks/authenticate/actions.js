import { setCookie, readCookie, eraseCookie } from '../../utils';

import {
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOGOUT_START,
    LOGOUT_SUCCESS,
    LOGOUT_ERROR,
    REFRESH_ACCESS_TOKEN_START,
    REFRESH_ACCESS_TOKEN_SUCCESS,
    REFRESH_ACCESS_TOKEN_ERROR,
} from './types';

const loginAccount = (email, password) => {
    return {
        type: LOGIN_START,
        payload: {
            email,
            password
        }
    }
}

const loginAccountSuccess = payload => {
    const {refreshToken} = payload.data

    setCookie('refreshToken', refreshToken);
  
    return {
        type: LOGIN_SUCCESS,
        payload
    }
}

const loginAccountError = err => {
    return {
        type: LOGIN_ERROR,
        err,
    }
}

const logoutAccount = () => {
    return {
        type: LOGOUT_START,
    }
}

const logoutAccountSuccess = () => {
    eraseCookie('refreshToken');
    eraseCookie('accessToken');

    return {
        type: LOGOUT_SUCCESS,
    }
}

const logoutAccountError = err => {
    eraseCookie('refreshToken');
    eraseCookie('accessToken');

    return {
        type: LOGOUT_ERROR,
        err,
    }
}

const refreshAccessToken = () => {
    return {
        type: REFRESH_ACCESS_TOKEN_START,
        payload: { refreshToken: readCookie('refreshToken') }
    }
}

const refreshAccessTokenSuccess = payload => {
    const {accessToken} = payload.data;
    
    setCookie('accessToken', accessToken);

    return {
        type: REFRESH_ACCESS_TOKEN_SUCCESS,
        payload
    }
}

const refreshAccessTokenError = err => {
    eraseCookie('refreshToken');
    eraseCookie('accessToken');
    
    return {
        type: REFRESH_ACCESS_TOKEN_ERROR,
        err
    }
}

// Actions to dispatch to React components
export {
    loginAccount,
    logoutAccount,
    refreshAccessToken
};

// Actions that are called within reducers.js file within Redux-Loop
export {
    loginAccountSuccess,
    loginAccountError,
    logoutAccountSuccess,
    logoutAccountError,
    refreshAccessTokenSuccess,
    refreshAccessTokenError
};