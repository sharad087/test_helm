import {
  createSelector
} from 'reselect';

const authenticateErrorSelector = state => state.authenticate.error;
const authenticateValidSelector = state => state.authenticate.valid;
const authenticateBusySelector = state => state.authenticate.busy;

export const selectAuthenticateError = createSelector(
  authenticateErrorSelector,
  error => error,
);

export const selectAuthenticateValid = createSelector(
  authenticateValidSelector,
  valid => valid,
)

export const selectAuthenticateBusy = createSelector(
  authenticateBusySelector,
  busy => busy
)