import { loop, Cmd } from "redux-loop";

import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_START,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  REFRESH_ACCESS_TOKEN_START,
  REFRESH_ACCESS_TOKEN_SUCCESS,
  REFRESH_ACCESS_TOKEN_ERROR
} from "./types";

import {
  loginAccountSuccess,
  loginAccountError,
  logoutAccountSuccess,
  logoutAccountError,
  refreshAccessTokenSuccess,
  refreshAccessTokenError
} from "./actions";

import * as api from "./api";
import { setCookie } from "../../utils";

const initialState = {
  busy: false,
  error: null,
  valid: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_START:
      const { email, password } = action.payload;
      return loop(
        { ...state, busy: true },
        Cmd.run(api.loginAccount, {
            successActionCreator: loginAccountSuccess,
            failActionCreator: loginAccountError,
            args: [email, password]
        })
      );

    case LOGIN_SUCCESS:
      setCookie("refreshToken", action.payload.data.refreshToken);

      return {
        ...state,
        busy: false,
        error: null,
        valid: true
      };

    case LOGIN_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response.data.message,
        valid: false
      };

    case LOGOUT_START:
      return loop(
        { ...state, busy: true },
        Cmd.action(logoutAccountSuccess())
      );

    case LOGOUT_SUCCESS:
      return {
        ...state,
        valid: false,
        busy: false,
        err: null,
      };

    case LOGOUT_ERROR:
      return {
        ...state,
        valid: false,
        busy: false,
        err: action.err.response
      };

    case REFRESH_ACCESS_TOKEN_START:
      return loop(
        { ...state, busy: true },
        Cmd.run(api.refreshAccessToken, {
          successActionCreator: refreshAccessTokenSuccess,
          failActionCreator: refreshAccessTokenError,
          args: [action.payload.refreshToken]
        })
      );
    case REFRESH_ACCESS_TOKEN_SUCCESS:
      setCookie("accessToken", action.payload.data.accessToken);

      return {
        ...state,
        busy: false,
        error: null,
        valid: true,
      };
    case REFRESH_ACCESS_TOKEN_ERROR:
      return {
        ...state,
        busy: false,
        error: action.err.response.data.message,
        valid: false
      };
    default:
      return state;
  }
};

export default reducer;