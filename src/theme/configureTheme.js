import { createMuiTheme } from '@material-ui/core/styles';

export default function configureStore (initialState) {
  const theme = createMuiTheme({
    palette: {
      primary: {
        light: '#4567d9',
        main: '#1742D0',
        dark: '#102e91',
        contrastText: '#fff'
      },
      secondary: {
        light: '#f73378',
        main: '#f50057',
        dark: '#ab003c',
        contrastText: '#fff'
      }
    },
    status: {
      danger: 'orange'
    }
  });

  return theme;
}
