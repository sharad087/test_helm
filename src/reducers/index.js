import { combineReducers } from 'redux-loop';
import { authenticate, account, activation, classes, notification, resetPassword, snackbar } from '../ducks';

import { LOGOUT_SUCCESS, LOGOUT_ERROR } from '../ducks/authenticate/types';

const appReducer = combineReducers({
  authenticate,
  account,
  activation,
  classes,
  notification,
  resetPassword,
  snackbar,
});

const rootReducer = (state, action) => {
  console.log(action.type);
  if(action.type === LOGOUT_SUCCESS || action.type === LOGOUT_ERROR){
    state = undefined;
  }

  return appReducer(state, action);
}

export default rootReducer;