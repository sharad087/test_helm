import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Switch } from 'react-router-dom';
import history from './history'
import routes from './routes/rootRoute';
import configureStore from './store/configureStore';
import configureTheme from './theme/configureTheme';
import configureAxios from './axios/configureAxios';
import { MuiThemeProvider } from '@material-ui/core/styles';
import registerServiceWorker from './registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';
import './styles.scss';

const store = configureStore();
const theme = configureTheme();

configureAxios();

ReactDOM.render(
  <Router history={history}>
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
          <Switch>
            {routes}
          </Switch>
      </MuiThemeProvider>
    </Provider>
  </Router>,
  document.getElementById('root')
);

registerServiceWorker();
