export default {
  updateProfile: {
    status :400,
    type: 'profile',
    message: "Profile cannot be updated",
  },
  requestEnrollment: {
    status: 400, 
    type: 'enrollment',
    message: "Unable to request enrollment",
  },
  refreshAccessToken: {
    status: 400,
    type: 'authenticate',
    message: "Error: Please sign in or refresh the page again",
  },
  getLobbyUrl: {
    status: 400,
    type: 'lobbyUrl',
    message: "Cannot open lobby",
  }
};
