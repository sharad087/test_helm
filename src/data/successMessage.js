export default {
  updateProfile: {
    status: 100,
    type: 'profile',
    message: "Profile information successfully updated"
  },
  requestEnrollment: {
    status: 100, 
    type: 'enrollment',
    message: "Enrollment requested",
  },
};
