import { createStore, compose, applyMiddleware } from 'redux';
import {install} from 'redux-loop';
import rootReducer from '../reducers';

export default function configureStore (initialState) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const enhancer = composeEnhancers(applyMiddleware(), install());
  
  const store = createStore(rootReducer, initialState, enhancer);
  return store;
}
