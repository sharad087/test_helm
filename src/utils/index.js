import moment from 'moment';

import currency from '../localization/currency';

export const readCookie = name => {
  var nameEQ = name + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};

export const setCookie = (name, value, days) => {
  var expires = '';
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; path=/; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
};

export const eraseCookie = name => {
  document.cookie = name + '=; Max-Age=-99999999; path=/;';
};

export const getDisplayName = WrappedComponent => {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
};

export const convertMilitaryToMinutes = militaryTime => {
  const hours = parseInt(militaryTime.slice(0, 2));
  const minutes = parseInt(militaryTime.slice(3, 5));

  return hours * 60 + minutes;
};

export const convertMinutesToMilitary = mins => {
  const hours = mins / 60;

  let minutes = mins % 60;

  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  return `${hours}:${minutes}`;
};

export const convertMinutesLabelToMinutes = minutesLabel => {
  return parseInt(minutesLabel.substring(0, minutesLabel.lastIndexOf(' ')));
};

export const capitalizeFirstLetter = str => {
  const uppercase = str.substring(0, 1).toUpperCase();
  const lowercase = str.substring(1, str.length);

  return `${uppercase}${lowercase}`;
};

export const capitalizeFirstLetters = str => {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

export const attachAccessToken = () => {
  return { headers: { Authorization: 'Bearer ' + readCookie('accessToken') } };
};

export const isEmptyString = str => {
  return str === '';
};

export const debounce = (func, wait, immediate) => {
  let timeout;

  return function executedFunction() {
    let context = this;
    let args = arguments;

    let later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    let callNow = immediate && !timeout;

    clearTimeout(timeout);

    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};

export const durationToString = (startTime, duration, daysOfWeek = []) => {
  const days = capitalizeFirstLetters(daysOfWeek.join(', ')) || "Day not available";
  const start = moment(`${startTime/60}:${startTime%60}`,"HH:m").format("hh:mm A")
  const end =  moment(`${(startTime+duration)/60}:${(startTime+duration)%60}`,"HH:m").format("hh:mm A")

  if (start !== "Invalid date" && end !== "Invalid date"){
    return `${days}, ${start} - ${end}`;
  }else{
    return "Time Not Available";
  }
}

export const lessonToString = (lessonCount) => {
  if (lessonCount){
    return `${lessonCount} lessons`;
  }else {
    return 'Number of lessons unavailable';
  }
}

export const startEndToString = (startDate, endDate) => {
  if(startDate && endDate && moment.isDate(new Date(startDate)) && moment.isDate(new Date(endDate))){
    return `${moment(startDate).format("MMM D, YYYY")} - ${moment(endDate).format("MMM D, YYYY")}`
  }else{
    return 'No Date Available';
  }
}

export const classSizeToString = (currentClassSize = "NA", maxClassSize = "NA") => {
  return `${maxClassSize - currentClassSize} of ${maxClassSize} seats remaining`;
}

export const priceToString = (price, currencyCode = "", prefix = true) => {
  const currencySymbol = currency[currencyCode.toUpperCase()] || "";

  if(price > 0){
    return prefix ? `${currencySymbol + price}` : `${price + currencySymbol}`;
  }else{
    return 'Unavailable'
  }
  
}

export const extractInitials = string => {
  const words = string.split();

  return words
    .map(word => word[0])
    .join()
    .toUpperCase();
};

export const validateEmail = email => {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
};

export const calculateGMT = timezone => {
  const tz = timezone.format();
  const GMT = tz.split('').slice(tz.length - 6, tz.length);
  return GMT.join('');
};