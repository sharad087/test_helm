import React, { Component } from 'react';
// import history from 'libs/history';

import NavigationSideBar from './NavigationSideBar';

export default class NavigationSideBarContainer extends Component {
  handleNavButtonClick = path => event => {
    this.props.history.push(path);
  };

  render() {
    const { Buttons, open } = this.props;

    return <NavigationSideBar Buttons={Buttons} open={open} />;
  }
}
