import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import MUIDrawer from '@material-ui/core/Drawer';

import scss from '../../scss/_common.scss';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    marginTop: 60,
    position: 'fixed'
  },
  drawerPaper: {
    width: drawerWidth,
    marginTop: scss.navBarHeight,
    backgroundColor: scss.immerseNavigation,
    color: 'black',
    padding: 8,
    paddingLeft: 0,
    border: 'none',
    boxShadow: `0px 0px 16px ${scss.immerseGreyWash}`
  }
});

const NavigationSideBar = props => {
  const { Buttons, open } = props;
  const { classes } = props;
  return (
    <MUIDrawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      {Buttons}
    </MUIDrawer>
  );
};

NavigationSideBar.defaultProps = {
  notificationCount: 0
};

NavigationSideBar.propTypes = {
  notificationCount: PropTypes.number
};

export default withStyles(styles, { withTheme: true })(NavigationSideBar);
