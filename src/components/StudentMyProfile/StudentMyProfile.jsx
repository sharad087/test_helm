import React from "react";
import ReactSVG from "react-svg";
import * as utils from '../../utils';
import momentTZ from "moment-timezone";

import DatePicker from "../_shared/DatePicker";
import { ProfileIcon } from "../_shared/ProfileIcon";
import Button from "../_shared/ImmerseButton";

import TextField from "@material-ui/core/TextField";
import { Select, MenuItem } from "@material-ui/core";
import OutlinedInput from "@material-ui/core/OutlinedInput";

const timeZones = momentTZ.tz
.names()
.map(name => {
  const tz = momentTZ.tz(name);
  const GMT = utils.calculateGMT(tz);

  if (GMT[0] === "+" || GMT[0] === "-") {
    return { name: `(GMT ${utils.calculateGMT(tz)}) ${name}`, value: name};
  } else {
    return { name: name, value: name};
  }
})
.sort((a,b) => {return a.name > b.name});

const timeZoneSelect = (value, values, name, onChange) => {
  return (
    <Select
      value={value || "Select Timezone"}
      onChange={onChange(name)}
      input={<OutlinedInput fullWidth margin="dense" />}
    >
      <MenuItem disabled value="Select Timezone">Select Timezone</MenuItem>
      {values.map(timeZone => (
        <MenuItem value={timeZone.value}>{timeZone.name}</MenuItem>
      ))}
    </Select>
  );
};

const StudentMyProfile = ({
  name,
  email,
  oldPassword,
  newPassword,
  dateOfBirth,
  timeZone,
  profileImage,
  onChange,
  toggleChangePassword,
  editPassword,
  onSubmit,
  setDOB,
  busy,
  onProfileImageChange
}) => {
  const TimeZoneSelect = timeZoneSelect(
    timeZone,
    timeZones,
    "timeZone",
    onChange
  );

  return (
    <div className="studentMyProfile">
      <div className="studentMyProfile__header">
        <h2 className="studentMyProfile__header__text">My Profile</h2>
      </div>

      <div className="studentMyProfile__body">
        <div className="studentMyProfile__body__header">
          <h3 className="studentMyProfile__body__header__text">
            Basic Information
          </h3>
        </div>

        <div className="studentMyProfile__body__detailsContainer">
          <div className="studentMyProfile__body__detailsContainer__profileContainer">
            <ProfileIcon name={name} src={profileImage} size="large" />

            <div >
              <label for="profile-image-upload" className="studentMyProfile__body__detailsContainer__profileContainer__edit">
                <ReactSVG src={require("../../images/edit.svg")} />
                <div>Edit profile photo</div>
              </label>
              <input className="studentMyProfile__body__inputFile" id="profile-image-upload" type="file" accept="image/png image/jpeg" onChange={onProfileImageChange} />
            </div>
          </div>

          <div className="studentMyProfile__body__infoContainer">
            <div className="studentMyProfile__body__infoContainer__info">
              <div className="studentMyProfile__body__infoContainer__info__title">
                Name
              </div>
              <TextField
                variant="outlined"
                margin="dense"
                value={name}
                onChange={onChange("name")}
                fullWidth
              />
            </div>
            <div className="studentMyProfile__body__infoContainer__info">
              <div className="studentMyProfile__body__infoContainer__info__title">
                Email
              </div>
              <TextField
                variant="outlined"
                margin="dense"
                value={email}
                onChange={onChange("email")}
                fullWidth
              />
            </div>
            <div className="studentMyProfile__body__infoContainer__info">
              <div className="studentMyProfile__body__infoContainer__info__title">
                {editPassword ? "Input old password" : "Current Password"}
              </div>
              <TextField
                variant="outlined"
                disabled={!editPassword}
                type="password"
                margin="dense"
                value={oldPassword}
                onChange={onChange("oldPassword")}
                placeholder={editPassword ? "" : "•".repeat(10)}
                fullWidth
              />
              {editPassword && (
                <div className="studentMyProfile__body__infoContainer__info__password">
                  <div className="studentMyProfile__body__infoContainer__info__title">
                    Input new Password
                  </div>
                  <TextField
                    variant="outlined"
                    disabled={!editPassword}
                    type="password"
                    margin="dense"
                    value={newPassword}
                    onChange={onChange("newPassword")}
                    fullWidth
                  />
                </div>
              )}
              <div
                className="studentMyProfile__body__infoContainer__info__select"
                onClick={toggleChangePassword}
              >
                {editPassword ? "Cancel" : "Change"}
              </div>
            </div>
            <div className="studentMyProfile__body__infoContainer__info">
              <div className="studentMyProfile__body__infoContainer__info__title">
                Date of Birth
              </div>
              <DatePicker
                value={dateOfBirth}
                handleDateChange={setDOB}
              />
            </div>
            <div className="studentMyProfile__body__infoContainer__info">
              <div className="studentMyProfile__body__infoContainer__info__title">
                Time Zone
              </div>
              {TimeZoneSelect}
            </div>
          </div>
        </div>

        <div className="studentMyProfile__body__buttonContainer">
          <Button onClick={onSubmit} disabled={busy} text="Save" size="sm" type="curved" />
        </div>
      </div>
    </div>
  );
};

export default StudentMyProfile;
