import React, { Component } from "react";
import { connect } from "react-redux";

import { LoadingWheel } from "../_shared/";

import StudentMyProfile from "./StudentMyProfile";
import {
  selectAccountProfile,
  selectAccountHasLoadedProfile,
  selectAccountIsLoadingProfile,
  selectAccountIsUpdatingProfile,
  selectAccountHasUpdatedProfile,
  selectAccountUpdateProfileError,
  selectAccountHasUpdatedProfileImage
} from "../../ducks/account/selectors";
import {
  getProfile,
  updateProfile,
  uploadProfileImage
} from "../../ducks/account/actions";

class StudentMyProfileContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: this.props.profile.name || "",
      email: this.props.profile.email || "",
      oldPassword: "",
      newPassword: "",
      dateOfBirth: this.props.profile.dateOfBirth || null,
      timeZone: this.props.profile.timeZonePref || "",
      profileImage: this.props.profile.profileImage || undefined,
      profileImageData: undefined,
      changePassword: false,
      uploadedProfileImage: false
    };

    this.onSave = this.onSave.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.toggleChangePassword = this.toggleChangePassword.bind(this);
    this.setDOB = this.setDOB.bind(this);
    this.handleProfileImageChange = this.handleProfileImageChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.hasLoadedProfile &&
      this.props.hasLoadedProfile !== prevProps.hasLoadedProfile
    ) {
      const {
        name,
        email,
        timeZonePref,
        dateOfBirth,
        profileImage
      } = this.props.profile;

      this.setState({
        name: name,
        email: email,
        timeZone: timeZonePref,
        dateOfBirth: dateOfBirth || null,
        profileImage: profileImage || undefined
      });
    }
  }

  generateProfileUpdateProps() {
    const {
      name,
      email,
      oldPassword,
      newPassword,
      timeZone,
      dateOfBirth
    } = this.state;
    let updateProps = {};

    //If only has an input field been changed, validate or reject. All altered fields must be valid before
    //submit (aka all or nothing).
    if (name !== this.props.profile.name) {
      //Validation condition, on fail return
      if (name.length) {
        updateProps.name = name;
      } else {
        return {};
      }
    }
    if (email !== this.props.profile.email) {
      if (email.length) {
        updateProps.email = email;
      } else {
        return {};
      }
    }
    if (oldPassword && newPassword) {
      updateProps.oldPassword = oldPassword;
      updateProps.newPassword = newPassword;
    }
    if (
      timeZone &&
      timeZone !== "Select Timezone" &&
      timeZone !== this.props.profile.timeZonePref
    ) {
      updateProps.timeZonePref = timeZone;
    }
    if (dateOfBirth && this.props.profile.dateOfBirth !== dateOfBirth ) {
      updateProps.dateOfBirth = dateOfBirth;
    }

    return updateProps;
  }

  onSave() {
    const updateProps = this.generateProfileUpdateProps();

    console.log("updatePRopsss", updateProps);

    if (Object.keys(updateProps).length > 0) {
      if (
        this.state.profileImage &&
        this.state.profileImage !== this.props.profile.profileImage
      ) {
        this.props
          .updateProfile(updateProps)
          .then(() =>
            this.props.uploadProfileImage(this.state.profileImageData)
          )
          .then(() => {
            if (this.props.hasUpdatedProfile) {
              window.location.reload();
            }
          });
      } else {
        this.props.updateProfile(updateProps).then(() => {
          if (this.props.hasUpdatedProfile) {
            window.location.reload();
          }
        });
      }
    } else if (
      this.state.profileImage &&
      this.state.profileImage !== this.props.profile.profileImage
    ) {
      //Updating only profile image
      console.log("only profile")
      this.props.uploadProfileImage(this.state.profileImageData).then(() => {
        if (this.props.hasUpdatedProfileImage) {
          window.location.reload();
        }
      });
    }
  }

  toggleChangePassword() {
    this.setState(prevState => ({
      changePassword: !prevState.changePassword,
      oldPassword: "",
      newPassword: ""
    }));
  }

  setDOB(date) {
    this.setState({
      dateOfBirth: date
    });
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleProfileImageChange(event) {
    const src = URL.createObjectURL(event.target.files[0]);
    const imgData = event.target.files[0];
    this.setState({
      profileImage: src,
      profileImageData: imgData,
      uploadProfileImage: true
    });
  }

  render() {
    const {
      name,
      email,
      timeZone,
      changePassword,
      dateOfBirth,
      oldPassword,
      newPassword,
      profileImage
    } = this.state;

    const { hasLoadedProfile } = this.props;
    return hasLoadedProfile ? (
      <StudentMyProfile
        name={name}
        email={email}
        profileImage={profileImage}
        oldPassword={oldPassword}
        newPassword={newPassword}
        dateOfBirth={dateOfBirth}
        timeZone={timeZone}
        onChange={this.handleChange}
        toggleChangePassword={this.toggleChangePassword}
        editPassword={changePassword}
        loading={this.props.loading}
        busy={this.props.isUpdatingProfile}
        onSubmit={this.onSave}
        setDOB={this.setDOB}
        onProfileImageChange={this.handleProfileImageChange}
      />
    ) : (
      <LoadingWheel />
    );
  }
}

const mapStateToProps = state => ({
  profile: selectAccountProfile(state),
  loading: selectAccountIsLoadingProfile(state),
  hasLoadedProfile: selectAccountHasLoadedProfile(state),
  isUpdatingProfile: selectAccountIsUpdatingProfile(state),
  hasUpdatedProfile: selectAccountHasUpdatedProfile(state),
  updateProfileError: selectAccountUpdateProfileError(state),
  hasUpdatedProfileImage: selectAccountHasUpdatedProfileImage(state)
});

const mapDispatchToProps = dispatch => ({
  getProfile: () => dispatch(getProfile()),
  updateProfile: updateProps => dispatch(updateProfile(updateProps)),
  uploadProfileImage: image => dispatch(uploadProfileImage(image))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StudentMyProfileContainer);
