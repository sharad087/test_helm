import React from "react";

import ReactSVG from "react-svg";
import Modal from "@material-ui/core/Modal";
import Input from "@material-ui/core/Input";
import Button from "../_shared/ImmerseButton";
import { withStyles } from "@material-ui/core";

import scss from "../../scss/_common.scss";

const ForgotPasswordModal = props => {
  const {
    open,
    onClose,
    onSubmit,
    busy,
    error,
    hasSendPasswordReset,
    email,
    onEmailChange
  } = props;

  let RenderConfirmation;

  if (hasSendPasswordReset && !busy && !error) {
    RenderConfirmation = (
      <React.Fragment>
        <div className={"forgot-password-sub-text"}>Please check your email for instructions to reset your password</div>
      </React.Fragment>
    );
  } else if (hasSendPasswordReset && !busy && error) {
    RenderConfirmation = (
      <React.Fragment>
        <div className={"forgot-password-sub-text"}>Error :(</div>
      </React.Fragment>
    );
  } else {
    RenderConfirmation = (
      <React.Fragment>
        <div className={"forgot-password-sub-text"}>
          No problem. Input your email address and we will send you instruction
          to reset your password.
        </div>
        <StyledInput
          value={email}
          onChange={onEmailChange}
          disableUnderline
          placeholder="Email"
        />
        <div className="forgot-password-submit">
        <Button
          
          onClick={onSubmit}
          disabled={busy}
          text="Send"
          size="sm"
          type="curved"
        />
        </div>
      </React.Fragment>
    );
  }

  return (
    <Modal
      className={"forget-password-modal"}
      onClose={onClose}
      open={open}
      disableAutoFocus
      BackdropProps={{
        invisible: true
      }}
    >
      <div className={"forget-password-container"}>
        <ReactSVG
          className={"close-icon"}
          src={require("../../images/close_icon.svg")}
          renumerateIRIElements={false}
          onClick={onClose}
        />

        <ReactSVG
        className={"decor"}
          src={require("../../images/question-mark.svg")}
          renumerateIRIElements={false}
        />

        <h3>Forgot your password?</h3>

        {RenderConfirmation}
      </div>
    </Modal>
  );
};

const StyledInput = withStyles({
  root: {
    width: "350px",
    fontSize: "14px",
    margin: "4px 0px",
    padding: "8px 16px",
    border: `1px solid ${scss.immerseGreyLight}`
  },
  disabled: {
    color: "black"
  }
})(Input);

ForgotPasswordModal.defaultProps = {
  open: false,
  busy: false,
  error: null,
  hasSendPasswordReset: false,
  email: '',
};

export default ForgotPasswordModal;
