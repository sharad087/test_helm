import React, {Component} from 'react';
import * as api from '../../ducks/account/api';
import * as utils from '../../utils';
import ForgotPasswordModal from './ForgotPasswordModal';

class ForgotPasswordModalContainer extends Component {
  constructor(props){
    super(props);

    this.state = {
      email: '',
      hasSendPasswordReset: false,
      busy: false,
      error: null,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
  }

  handleEmailChange(event){
    this.setState({email: event.target.value});
  }

  handleSubmit(){
    if(this.state.email.length > 0 && utils.validateEmail(this.state.email)){
      this.setState({busy: true});
      api.sendResetPassword(this.state.email)
        .then(response => {
          console.log(response);
          this.setState({hasSendPasswordReset: true, busy: false, error: null});
        })
        .catch(error => {
          console.log(error);
          this.setState({hasSendPasswordReset: true, busy: false, error: error});
        })
    }
  }

  render(){
    const {open, onClose} = this.props;
    const {email, busy, error, hasSendPasswordReset} = this.state;

    return(
      <ForgotPasswordModal
        open={open}
        onClose={onClose}
        email={email}
        onEmailChange={this.handleEmailChange}
        onSubmit={this.handleSubmit}
        busy={busy}
        hasSendPasswordReset={hasSendPasswordReset}
        error={error}
      />
    )
  }
}

export default ForgotPasswordModalContainer;