import React from "react";
import ReactSVG from "react-svg";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Input from "@material-ui/core/Input";
import { withStyles } from "@material-ui/core";

import Button from "../_shared/Button/Button";

import { ForgotPasswordModal } from "../ForgotPasswordModal";

const Login = props => {
  const {
    email,
    password,
    error,
    onLogin,
    onChange,
    openForgotPasswordModal,
    openModal,
    closeModal
  } = props;

  return (
    <div className="login">
      <ReactSVG
        src={require("../../images/immerse_logo.svg")}
        evalScripts="always"
        renumerateIRIElements={false}
      />
      <div className="login__title">Log in to access Immerse</div>

      <div className="login__input">
        <div className="login__input__errorMessage">{error}</div>
        <div className="login__input__form">
          <form onSubmit={onLogin}>
            <StyledPaper>
              <StyledInput
                id="email"
                value={email}
                onChange={onChange}
                required
                disableUnderline
                placeholder={"Email"}
              />
              <Divider />
              <StyledInput
                id="password"
                value={password}
                onChange={onChange}
                required
                type="password"
                disableUnderline
                placeholder={"Password"}
              />
            </StyledPaper>

            <LoginButton type="submit" color="primary" variant={"contained"}>
              Log In
            </LoginButton>
          </form>
        </div>
      </div>

      <div className="login__footer">
        <StyledFormControlLabel control={<Checkbox />} label="Remember Me" />
        <FogotPasswordButton
          id="openForgotPasswordModal"
          onClick={openModal}
          disableFocusRipple
          disableRipple
        >
          Forgot Password?
        </FogotPasswordButton>
      </div>

      {openForgotPasswordModal && (
        <ForgotPasswordModal
          open={openForgotPasswordModal}
          onClose={() => closeModal("openForgotPasswordModal")}
        />
      )}
    </div>
  );
};

export default Login;

const LoginButton = withStyles({
  root: {
    position: "absolute",
    zIndex: 1,
    width: "80px",
    right: "0",
    top: "50%",
    transform: "translate(50%, -50%)",
    boxShadow: "0 18px 36px 0px #E1EAFC"
  }
})(Button);

const FogotPasswordButton = withStyles({
  root: {
    background: "transparent",
    color: "#686868",
    "&:hover": {
      backgroundColor: "transparent"
    },
    "&:active": {
      backgroundColor: "transparent"
    }
  },
  label: {
    color: "#4A90E2",
    textTransform: "capitalize",
    zIndex: -1
  }
})(Button);

const StyledFormControlLabel = withStyles({
  label: {
    color: "#686868"
  }
})(FormControlLabel);

const StyledInput = withStyles({
  root: {
    width: "100%",
    margin: 8
  }
})(Input);

const StyledPaper = withStyles({
  root: {
    boxShadow: "0 18px 36px 0px #E1EAFC"
  }
})(Paper);
