import React, { Component } from "react";
import { connect } from "react-redux";
import Login from "./Login";

import { loginAccount } from "../../ducks/authenticate/actions";
import { selectAuthenticateError } from "../../ducks/authenticate/selectors";

class LoginContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      error: "",

      openForgotPasswordModal: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.validateLogin = this.validateLogin.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  validateLogin() {
    const { email, password } = this.state;
    let error = "";

    if (!email && !password) {
      error = "Email and password required";
    } else if (!email) {
      error = "Email required";
    } else if (!password) {
      error = "Password required";
    }

    return error;
  }

  handleLogin(event) {
    const { email, password } = this.state;
    const error = this.validateLogin();
    this.setState({error: error})

    if (!error) {
      this.props.onLoginSubmit(email, password);
    }

    event.preventDefault();
  }

  handleOpenModal(event) {
    this.setState({ [event.target.id]: true });
  }

  handleCloseModal(modalName) {
    this.setState({ [modalName]: false });
  }

  render() {
    const { email, password, error, openForgotPasswordModal } = this.state;
    const errors = error || this.props.loginError;
    
    return <Login
      email={email}
      password={password}
      error={errors}
      onLogin={this.handleLogin}
      onChange={this.handleChange}
      openForgotPasswordModal={openForgotPasswordModal}
      openModal={this.handleOpenModal}
      closeModal={this.handleCloseModal}
    />;
  }
}

const mapStateToProps = state => {
  return {
    loginError: selectAuthenticateError(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoginSubmit: (email, password) => dispatch(loginAccount(email, password)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);