import React from "react";

import { NavigationBar } from "../NavigationBar";
import { NavigationSideBar } from "../NavigationSideBar";

const Dashboard = props => {
  const { navigationButtons, openDrawer, onToggleDrawer } = props;
  return (
    <div className={"dashboard"}>
      <NavigationBar onToggleDrawer={onToggleDrawer}/>
      <div className="dashboard__contentContainer">
        <div className="dashboard__sideBar">
          <NavigationSideBar Buttons={navigationButtons} open={openDrawer} />
        </div>
        <div className={openDrawer ? "dashboard__sidebar--shifted" : "dashboard__content"}>
          { props.children}
        </div>
      </div>
    </div>
  );
};

export default Dashboard;