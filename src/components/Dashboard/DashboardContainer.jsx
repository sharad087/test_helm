import React, { Component } from "react";
import { connect } from "react-redux";
import Dashboard from "./Dashboard";
import route from "../../routes/dashboardRoute";
import { NavigationButton } from "../NavigationButton";
import { Switch } from "react-router-dom";

import { Snackbar } from '../_shared';

import { resetSnackbars } from '../../ducks/snackbar/actions';
import { selectError, selectSuccess } from "../../ducks/snackbar/selectors";
import { getProfile } from "../../ducks/account/actions";
import { selectAccountHasLoadedProfile } from "../../ducks/account/selectors";

const routeIcons = {
  "My Courses": require('../../images/presentation.svg'),
  "Book a Course": require('../../images/teach.svg'),
}

class DashboardContainer extends Component {
  constructor(props){
    super(props);
    console.log("Dashboard constructor");
    this.props.getProfile();

    this.state = {
      openDrawer : true,
    }

    this.handleToggleDrawer = this.handleToggleDrawer.bind(this);
  }

  handleNavButtonClick = path => event => {
    this.props.history.push(path);
  };

  handleToggleDrawer() {
    this.setState(prevState => ({
      openDrawer: !prevState.openDrawer
    }))
  }

  render() {
    const { openDrawer } = this.state;
    const { loadedProfile } = this.props;

    const dashboardRoute = route.filter(route => (route.key !== "My Profile" && route.key !== "Redirect"));

    const buttons = dashboardRoute.map((route, index) => {
      return (
        <NavigationButton
          key={index}
          text={route.key}
          icon={routeIcons[route.key]}
          selected={this.props.location.pathname === route.props.path}
          onClick={this.handleNavButtonClick(route.props.path)}
        />
      );
    });

    return (
      <Dashboard
        openDrawer={openDrawer}
        onToggleDrawer={this.handleToggleDrawer}
        navigationButtons={buttons}
      >
        {loadedProfile && <React.Fragment>
          <Switch>
            {route}
          </Switch>
          <Snackbar
            open={this.props.snackbarError.open}
            message={this.props.snackbarError.message}
            vertical='bottom'
            horizontal='center'
            onClose={this.props.resetSnackbars}
            type='error'
            autoHideDuration={2500}
          />
          <Snackbar
            open={this.props.snackbarSuccess.open}
            message={this.props.snackbarSuccess.message}
            vertical='bottom'
            horizontal='center'
            onClose={this.props.resetSnackbars}
            type='success'
            autoHideDuration={2500}
          />
          </React.Fragment>}
      </Dashboard>
    );
  }
}

const mapStateToProps = state => ({
  snackbarError: selectError(state),
  snackbarSuccess: selectSuccess(state),
  loadedProfile: selectAccountHasLoadedProfile(state),
})

const mapDispatchToProps = dispatch => ({
  getProfile: () => dispatch(getProfile()),
  resetSnackbars: () => dispatch(resetSnackbars()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardContainer);