import React from 'react';
import ReactSVG from 'react-svg';
import PropTypes from 'prop-types';

import MenuIcon from "@material-ui/icons/Menu";
import { NavigationProfileButton } from '../NavigationProfileButton';
import {NavigationNotificationButton} from '../NavigationNotificationButton';

const NavigationBar = (props) => {
  const { onToggleDrawer } = props;
  return (
    <div className={`nav-bar ${props.className}`}>
      <div className={'nav-bar__routes'}>
        <ReactSVG svgClassName={'nav-bar__logo-svg'}
          src={require('../../images/immerse_logo.svg')} />
        <div className="nav-bar__menu">
          <MenuIcon onClick={onToggleDrawer}/>
        </div>

        <div className={'nav-bar__children'}>
          {props.children}
        </div>
      </div>

      <NavigationNotificationButton notificationCount={props.notificationCount} />
      <NavigationProfileButton/>    
    </div>
  );
};

NavigationBar.defaultProps = {
  notificationCount: 0
};

NavigationBar.propTypes = {
  notificationCount: PropTypes.number
};

export default NavigationBar;
