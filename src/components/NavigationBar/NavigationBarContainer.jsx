import React, { Component } from 'react';
import NavigationBar from './NavigationBar';

export default class NavigationBarContainer extends Component {
  render () {
    const { onToggleDrawer } = this.props;

    return (
      <NavigationBar
        onToggleDrawer={onToggleDrawer}
      >
        {this.props.children}
      </NavigationBar>
    );
  }
}
