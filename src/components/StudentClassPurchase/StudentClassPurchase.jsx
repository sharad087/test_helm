import React from 'react';

const StudentClassPurchase = (props) => (
    <div className="studentClassPurchase">
        {React.Children.map(props.children, (child, index) => {
            if(index === 0){
                return (
                    <div className="studentClassPurchase__headerContainer">
                        <h2 className="studentClassPurchase__header">Book a Course</h2>
                        {child}          
                    </div>
                )
            }else{
                return child;
            }
        })}
    </div>
)

export default StudentClassPurchase;