import React, { PureComponent } from "react";
import * as utils from "../../utils";
import { connect } from "react-redux";
import {
  getStoreClasses,
  fuzzySearchStoreClasses
} from "../../ducks/classes/actions";
import {
  selectClassesStoreClasses,
  selectClassesError,
  selectClassesBusy,
  selectClassesLoaded,
  selectClassesStoreClassesNoMore,
  selectClassesIsFuzzySeach,
  selectClassesIsStoreClassesInitialized,
  selectClassesStoreClassesQuery,
  selectClassesStoreClassesSkip
} from "../../ducks/classes/selectors";
import ClassPurchaseGrid from "./ClassPurchaseGrid/ClassPurchaseGrid";
import StudentClassPurchase from "./StudentClassPurchase";
import StudentClassPurchaseFilter from "./StudentClassPurchaseFilters/StudentClassPurchaseFilter";

import { ClassPurchaseModal } from "../ClassPurchaseModal";
import { LoadingWheel } from "../_shared";
import SearchNoResultCard from "./_shared/SearchNoResultCard";
import ErrorNoResultCard from "./_shared/ErrorNoResultCard";

class StudentClassPurchaseContainer extends PureComponent {
  constructor(props) {
    super(props);
    if (!this.props.initialized) {
      this.props
        .getStoreClasses()
        // .then(() => this.setState({ noMore: this.props.noMore }));
    }

    this.state = {
      query: {
        string: this.props.query.string || "",
        daysOfWeek: this.props.query.daysOfWeek || "",
        times: []
      },
      fuzzy: false,
      purchaseClassOpen: false,
      availabilityOpen: false,
      purchaseClassData: {},
      // noMore: false,
      isDebouncing: false
    };

    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handleDayChange = this.handleDayChange.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);

    this.purchaseClassOnPress = this.purchaseClassOnPress.bind(this);
    this.purchaseClassClose = this.purchaseClassClose.bind(this);

    this.fuzzySearch = this.fuzzySearch.bind(this);
    this.handleFuzzySearch = this.handleFuzzySearch.bind(this);
    this.fuzzySearchDebounce = utils.debounce(this.fuzzySearchDebounce, 350);

    this.toggleAvailabilityPopper = this.toggleAvailabilityPopper.bind(this);
    this.handleAvailabilityPopperClose = this.handleAvailabilityPopperClose.bind(this);

    this.onScroll = this.onScroll.bind(this);

    this.renderClassesGrid = this.renderClassesGridAndModal.bind(this);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll, false);
  }

  onScroll() {
    if (
      !this.props.busy &&
      !this.props.noMore &&
      !this.props.error &&
      window.innerHeight + window.scrollY >=
        document.getElementById("root").offsetHeight - 1
    ) {
      // if (this.props.isFuzzySearch) {
      if (this.state.fuzzy){
        this.fuzzySearch(15, this.props.classesData.length, this.state.query);
      } else {
        this.getStoreClasses(15, this.props.classesData.length, this.state.query);
      }
    }
  }

  handleQueryChange = event => {
    const value = event.target.value;
    this.setState(prevState => ({
      query: {
        ...prevState.query,
        string: value
      },
      fuzzy: value ? true : false,
    }));
  };

  handleDayChange(event, day) {
    this.setState(prevState => ({
      query: { ...prevState.query, daysOfWeek: day !== null ? day : "" }
    }));

    // if (this.props.isFuzzySearch) {
    if (this.state.fuzzy){
      this.fuzzySearch(15, 0, {
        ...this.state.query,
        daysOfWeek: day !== null ? day : ""
      });
    } else {
      this.getStoreClasses(15, 0, {
        ...this.state.query,
        daysOfWeek: day !== null ? day : ""
      });
    }
  }

  handleTimeChange(event, times) {
    this.setState(prevState => ({
      query: { ...prevState.query, times: times === null ? [] : times }
    }));

    // if (this.props.isFuzzySearch) {
    if (this.state.fuzzy){
      this.fuzzySearch(15, 0, {
        ...this.state.query,
        times: times === null ? [] : times
      });
    } else {
      this.getStoreClasses(15, 0, {
        ...this.state.query,
        times: times === null ? [] : times
      });
    }
  }

  getStoreClasses(limit = 15, skip = 0, query = {}) {
    const filteredQuery = {
      ...(query.daysOfWeek &&
        query.daysOfWeek.length !== 0 && { daysOfWeek: query.daysOfWeek })
    };

    if (query.times && query.times.length !== 0) {
      const startTimes = [];
      const endTimes = [];

      query.times.forEach(([startTime, endTime]) => {
        startTimes.push(startTime);
        endTimes.push(endTime);
      });

      filteredQuery["startTimes"] = startTimes.join(",");
      filteredQuery["endTimes"] = endTimes.join(",");
    }

    this.props
      .getStoreClasses(limit, skip, filteredQuery)
      // .then(() => this.setState({ noMore: this.props.noMore }));
  }

  handleFuzzySearch(event) {
    this.handleQueryChange(event);
    this.setState({ isDebouncing: true }, () =>
      this.fuzzySearchDebounce(15, 0, this.state.query)
    );
  }

  fuzzySearchDebounce(limit, skip, query) {
    this.fuzzySearch(limit, skip, query).then(() =>
      this.setState({ isDebouncing: false })
    );
  }

  async fuzzySearch(limit = 15, skip = 0, query = {}) {
    // if (!this.props.busy) {
      const filteredQuery = {
        ...(query.daysOfWeek &&
          query.daysOfWeek.length !== 0 && { daysOfWeek: query.daysOfWeek }),
        ...(this.state.query.string.length > 0 && {
          title: query.string,
          subtitle: query.string,
          language: query.string
        })
      };

      if (query.times && query.times.length !== 0) {
        const startTimes = [];
        const endTimes = [];

        query.times.forEach(([startTime, endTime]) => {
          startTimes.push(startTime);
          endTimes.push(endTime);
        });

        filteredQuery["startTimes"] = startTimes.join(",");
        filteredQuery["endTimes"] = endTimes.join(",");
      }

      if (this.state.query.string.length === 0) {
        this.getStoreClasses(limit, skip, query);
      } else {
        this.props
          .fuzzySearchStoreClasses(limit, skip, filteredQuery)
          // .then(() => this.setState({ noMore: this.props.noMore }));
      }
    // }
  }

  purchaseClassOnPress(course) {
    this.setState({ purchaseClassOpen: true, purchaseClassData: course });
  }

  purchaseClassClose() {
    this.setState({ purchaseClassOpen: false });
  }

  toggleAvailabilityPopper() {
    console.log('toggle')
    this.setState(prevState => ({
      availabilityOpen: !prevState.availabilityOpen
    }));
  }

  handleAvailabilityPopperClose() {
    console.log("close")
    this.setState({
      availabilityOpen: false
    });
  }

  renderClassesGridAndModal() {
    const { busy, loaded, classesData, skip, error } = this.props;
    const { purchaseClassOpen, purchaseClassData, isDebouncing } = this.state;

    if ((busy && skip === 0) || isDebouncing) {
      return <LoadingWheel />;
    } else if (loaded && classesData.length === 0) {
      return <SearchNoResultCard title="No results found" subtitle="Please adjust your search"/>
    } else if (!error && classesData.length > 0) {
      return (
        <div>
          <ClassPurchaseGrid
            loading={this.props.busy}
            classes={this.props.classesData}
            onClassClick={this.purchaseClassOnPress}
          />

          <ClassPurchaseModal
            open={purchaseClassOpen}
            onClose={this.purchaseClassClose}
            course={purchaseClassData}
          />
        </div>
      );
    } else {
      return <ErrorNoResultCard title="Error :(" subtitle="Please try again"/>
    }
  }

  render() {
    const {
      query: { string, daysOfWeek, times },
      location,
      availabilityOpen
    } = this.state;

    const ClassesGridAndModal = this.renderClassesGridAndModal();

    return (
      <StudentClassPurchase>
        <StudentClassPurchaseFilter
          query={string}
          time={times}
          day={daysOfWeek}
          location={location}
          onSearchChange={this.handleFuzzySearch}
          availabilityOpen={availabilityOpen}
          handleAvailabilityOpen={this.toggleAvailabilityPopper}
          handleAvailabilityClose={this.handleAvailabilityPopperClose}
          onTimeChange={this.handleTimeChange}
          onDayChange={this.handleDayChange}
        />
        {ClassesGridAndModal}
      </StudentClassPurchase>
    );
  }
}

const mapStateToProps = state => {
  return {
    classesData: selectClassesStoreClasses(state),
    error: selectClassesError(state),
    busy: selectClassesBusy(state),
    loaded: selectClassesLoaded(state),
    noMore: selectClassesStoreClassesNoMore(state),
    // isFuzzySearch: selectClassesIsFuzzySeach(state),
    initialized: selectClassesIsStoreClassesInitialized(state),
    query: selectClassesStoreClassesQuery(state),
    skip: selectClassesStoreClassesSkip(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getStoreClasses: (limit, skip, query) =>
      dispatch(getStoreClasses(limit, skip, query)),
    fuzzySearchStoreClasses: (limit, skip, query) =>
      dispatch(fuzzySearchStoreClasses(limit, skip, query))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StudentClassPurchaseContainer);