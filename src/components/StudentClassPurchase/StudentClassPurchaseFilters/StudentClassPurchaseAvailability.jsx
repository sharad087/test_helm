import React from "react";
import ReactSVG from "react-svg";

import scss from "../../../scss/_common.scss";

import { Button } from "../../_shared";

import { withStyles, ClickAwayListener } from "@material-ui/core";
import Popper from "@material-ui/core/Popper";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";

const selectedStyle = {
  backgroundColor: scss.immerseBlueOpacity,
  color: "black"
};

const days = [
  { title: "Mon", value: "monday" },
  { title: "Tue", value: "tuesday" },
  { title: "Wed", value: "wednesday" },
  { title: "Thu", value: "thursday" },
  { title: "Fri", value: "friday" },
  { title: "Sat", value: "saturday" },
  { title: "Sun", value: "sunday" }
];

const timeOfDay = [
  {
    title: "06 - 09",
    value: [360, 540],
    src: require("../../../images/sunrise-fog-30.svg")
  },
  {
    title: "09 - 12",
    value: [540, 720],
    src: require("../../../images/sunrise-fog-30.svg")
  },
  {
    title: "12 - 15",
    value: [720, 900],
    src: require("../../../images/brightness-46.svg")
  },
  {
    title: "15 - 18",
    value: [900, 1080],
    src: require("../../../images/brightness-46.svg")
  },
  {
    title: "18 - 21",
    value: [1080, 1260],
    src: require("../../../images/sunset-fog-30.svg")
  },
  {
    title: "21 - 24",
    value: [1260, 1440],
    src: require("../../../images/sunset-fog-30.svg")
  },
  {
    title: "00 - 03",
    value: [0, 180],
    src: require("../../../images/moon.svg")
  },
  {
    title: "03 - 06",
    value: [180, 360],
    src: require("../../../images/moon.svg")
  }
];

const StudentClassPurchaseAvailability = props => (
  <div className="studentClassPurchaseFilters__availability">
    <StyledButton
      className="studentClassPurchaseFilters__availability"
      onClick={props.onClick}
      buttonRef={node => {
        this.anchorEl = node;
      }}
      disableRipple
      modifiers={{
        flip: {
          enabled: false
        },
        preventOverflow: {
          enabled: false,
          boundariesElement: "scrollParent"
        }
      }}
    >
      <ReactSVG
        className="studentClassPurchaseFilters__inputAdornment"
        src={require("../../../images/calendar-grid-61.svg")}
      />
      <span>Availability</span>
    </StyledButton>

    <Popper
      className="studentClassPurchaseFilters__popperContainer"
      open={props.availabilityOpen}
      modifiers={{
        preventOverflow: {
          enabled: false,
          boundariesElement: "scrollParent"
        },
        flip: {
          enabled: false
        },
        hide: {
          enabled: false
        }
      }}
      anchorEl={this.anchorEl}
      placement="bottom-start"
    >
      <ClickAwayListener onClickAway={props.onClose} mouseEvent="onClick">
        <div className="studentClassPurchaseFilters__popper">
          <div className="studentClassPurchaseFilters__popper__title">
            Time of Day
          </div>
          <StyledToggleTimeButtonGroup
            value={props.time}
            onChange={props.onTimeChange}
          >
            {timeOfDay.map(({ title, value, src }) => (
              <StyledToggleTimeButton
                value={value}
                disableRipple
                disableFocusRipple
                style={props.time.includes(value) ? selectedStyle : {}}
              >
                <ReactSVG src={src} />
                <div className="studentClassPurchase__toggleButton__time">
                  {title}
                </div>
              </StyledToggleTimeButton>
            ))}
          </StyledToggleTimeButtonGroup>
          <div className="studentClassPurchaseFilters__popper__title">Day</div>

          <StyledToggleButtonGroup
            exclusive
            value={props.day}
            onChange={props.onDayChange}
          >
            {days.map(({ title, value }) => (
              <StyledToggleDayButton
                disableRipple
                disableFocusRipple
                value={value}
                style={props.day === value ? selectedStyle : {}}
              >
                {title}
              </StyledToggleDayButton>
            ))}
          </StyledToggleButtonGroup>
        </div>
      </ClickAwayListener>
    </Popper>
  </div>
);

StudentClassPurchaseAvailability.defaultProps = {
  time: [],
  day: ""
};

const StyledButton = withStyles({
  root: {
    borderRadius: 0,
    width: "100%",
    "&:hover": {
      background: "none"
    }
  },
  label: {
    justifyContent: "flex-start"
  }
})(Button);

const StyledToggleButtonGroup = withStyles({
  root: {
    width: "100%",
    marginBottom: scss.gMargin
  },
  selected: {
    boxShadow: "none"
  }
})(ToggleButtonGroup);

const StyledToggleTimeButtonGroup = withStyles({
  root: {
    width: "100%",
    marginBottom: scss.gMargin,
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr 1fr"
  },
  selected: {
    boxShadow: "none"
  }
})(ToggleButtonGroup);

const StyledToggleDayButton = withStyles({
  root: {
    color: "black",
    textTransform: "none",
    fontSize: "10px",
    padding: "0",
    margin: "0",

    "&:nth-child(-n+7)": {
      borderWidth: "1px 0px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    },

    "&:nth-child(7)": {
      borderWidth: "1px 1px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    }
  }
})(ToggleButton);

const StyledToggleTimeButton = withStyles({
  root: {
    width: "85px",
    color: "black",
    textTransform: "none",
    fontSize: "10px",
    padding: "0",
    margin: "0",
    backgroundColor: "white",
    borderRadius: "0px",

    "&:nth-child(-n+4)": {
      borderWidth: "1px 0px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    },

    "&:nth-child(4)": {
      borderWidth: "1px 1px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    },

    "&:nth-last-child(-n+4)": {
      borderWidth: "0px 0px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    },

    "&:last-child": {
      borderWidth: "0px 1px 1px 1px",
      borderStyle: "solid",
      borderColor: scss.immerseGreyWash
    }
  }
})(ToggleButton);

export default StudentClassPurchaseAvailability;
