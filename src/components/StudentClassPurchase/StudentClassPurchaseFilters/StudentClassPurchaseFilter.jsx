import React from 'react';

import StudentClassPurchaseSearch from './StudentClassPurchaseSearch';
import StudentClassPurchaseAvailability from './StudentClassPurchaseAvailability';
import {VerticalDivider} from '../../_shared';

const StudentClassPurchaseFilter = (props) => (
    <div className="studentClassPurchaseFilters">
        <StudentClassPurchaseSearch value={props.query} onChange={props.onSearchChange}/>
        <VerticalDivider className="studentClassPurchaseFilters__divider"/>
        <StudentClassPurchaseAvailability
            availabilityOpen={props.availabilityOpen}
            time={props.time}
            day={props.day}
            onClick={props.handleAvailabilityOpen}
            onClose={props.handleAvailabilityClose}
            onTimeChange={props.onTimeChange}
            onDayChange={props.onDayChange}
        />
    </div>
)

export default StudentClassPurchaseFilter;