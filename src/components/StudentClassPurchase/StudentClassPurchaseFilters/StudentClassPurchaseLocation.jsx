import React from 'react';
import ReactSVG from 'react-svg';

import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText'; 

const StudentClassPurchaseLocation = (props) => (
    <Select
        className="studentClassPurchaseFilters__location"
        multiple
        value={props.location}
        onChange={props.onChange}
        IconComponent="none"
        input={
            <InputBase  
                startAdornment={(
                    <InputAdornment className="studentClassPurchaseFilters__inputAdornment">
                        <ReactSVG src={require('../../../images/location-black.svg')}/>
                    </InputAdornment>
                )}
            />            
        }
        renderValue={selected => {return selected.length > 1 ? (selected.filter(location => location !== "Location")).join(', ') : "Location" }}
    >
        {props.locations.map((location) => (
            <MenuItem key={location} value={location}>
                <Checkbox checked={props.location.indexOf(location) > -1}/>
                <ListItemText primary={location}/>
            </MenuItem>
        ))}
    </Select>
)

StudentClassPurchaseLocation.defaultProps = {
    locations: [],
}

export default StudentClassPurchaseLocation;