import React from 'react';
import ReactSVG from 'react-svg';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';

const StudentClassPurchaseSearch = (props) => (
    <InputBase
        name="string"
        value={props.value}
        onChange={props.onChange}
        className="studentClassPurchaseFilters__search"
        startAdornment={
            <InputAdornment className="studentClassPurchaseFilters__inputAdornment">
                <ReactSVG src={require('../../../images/search.svg')}/>
            </InputAdornment>
        }
    />
)

export default StudentClassPurchaseSearch;