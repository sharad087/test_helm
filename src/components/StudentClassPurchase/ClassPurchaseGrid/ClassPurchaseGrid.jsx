import React from "react";
import ClassPurchaseCard from "./ClassPurchaseCard";

import { LoadingWheel } from "../../_shared";

const ClassPurchaseGrid = props => (
  <div>
    <div className="classPurchaseGridContainer">
      <div className="classPurchaseGrid">
        {props.classes.map((course, index) => {
          return (
            <div className="classPurchaseCardContainer">
              <ClassPurchaseCard
                key={index}
                course={course}
                onClassClick={props.onClassClick}
              />
            </div>
          );
        })}
      </div>
    </div>
    {props.loading && <LoadingWheel />}
  </div>
);

export default ClassPurchaseGrid;
