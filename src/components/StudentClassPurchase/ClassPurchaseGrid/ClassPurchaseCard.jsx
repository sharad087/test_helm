import React from "react";
import ReactSVG from "react-svg";
import * as utils from "../../../utils";

import Tooltip from "@material-ui/core/Tooltip";

import IconListItem from "../../_shared/IconListItem/IconListItem";
import ProfileIcon from "../../_shared/ProfileIcon/ProfileIcon";
import { Card } from "../../_shared";

const ClassPurchaseCard = ({ onClassClick, course }) => {
  const {
    title,
    subtitle,
    daysOfWeek,
    time,
    lessonCount,
    startDate,
    endDate,
    duration,
    location,
    currentSize,
    maxSize
  } = course;

  let teacherName;
  let teacherProfileImage = undefined;
  
  if (course.hasOwnProperty("teacher") && course.teacher) {
    const { name, profileImage } = course.teacher;
    teacherName = name;
    teacherProfileImage = profileImage;
  }

  const durationString = utils.durationToString(time, duration, daysOfWeek);
  const lessonString = utils.lessonToString(lessonCount);
  const startEndString = utils.startEndToString(startDate, endDate);
  const locationString = location || "No Location Available";
  const classSizeString = utils.classSizeToString(
    currentSize,
    maxSize
  );

  return (
    <Card
      padding="large"
      onClick={() => {
        onClassClick(course);
      }}
    >
      <div className="classPurchaseCard">
        <div className="classPurchaseCard__header">
          <div className="classPurchaseCard__header__subtitle">{subtitle}</div>
          <h2 className="classPurchaseCard__header__title">{title}</h2>
        </div>

        <div className="classPurchaseCard__infoContainer">
          <IconListItem
            text={durationString}
            src={require("../../../images/time-clock.svg")}
          />
          <IconListItem
            text={lessonString}
            src={require("../../../images/teach-blue.svg")}
          />
          <IconListItem
            text={startEndString}
            src={require("../../../images/check-in.svg")}
          />
          <IconListItem
            text={locationString}
            src={require("../../../images/pin.svg")}
          />
          <IconListItem
            text={classSizeString}
            src={require("../../../images/students.svg")}
          />
        </div>

        <div className="classPurchaseCard__teacherContainer">
          <Tooltip title={teacherName} placement="top">
            <div>
              <ProfileIcon
                size="small"
                name={teacherName}
                src={teacherProfileImage}
              />
            </div>
          </Tooltip>
        </div>
      </div>
      <ReactSVG
        src={require("../../../images/waves-409.svg")}
        svgClassName="classPurchaseCard__waves"
      />
    </Card>
  );
};

ClassPurchaseCard.defaultProps = {
  course: {
    title: "",
    subtitle: "",
    time: "",
    lessonCount: "",
    duration: "",
    location: "No Location Available",
    classSize: "",
  }
};

export default ClassPurchaseCard;
