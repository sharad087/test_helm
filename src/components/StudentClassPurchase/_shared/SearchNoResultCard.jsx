import React from "react";
import ReactSVG from "react-svg";

import { NoResultCard } from "../../_shared";

const SearchNoResultCard = props => {
  const { title, subtitle } = props;

  return (
    <NoResultCard className="studentClassPurchaseNoResultCard">
      <ReactSVG src={require("../../../images/virtual-reality-1.svg")} />

      <div className="studentClassPurchaseNoResultCard__info">
        <h3>{title}</h3>
        <div className="studentClassPurchaseNoResultCard__subtitle">{subtitle}</div>
      </div>
    </NoResultCard>
  );
};

SearchNoResultCard.defaultProps = {
  title: "",
  subtitle: ""
};

export default SearchNoResultCard;
