import React from "react";
import { Link } from "react-router-dom";
import Button from "../_shared/ImmerseButton";

import { withStyles } from "@material-ui/core";
import Input from "@material-ui/core/Input";

import scss from "../../scss/_common.scss";

const ResetPassword = props => {
  const {
    password,
    confirmPassword,
    onChange,
    onSubmit,
    busy,
    error,
    reset
  } = props;

  let RenderResetPassword;

  if (reset && !busy && !error) {
    RenderResetPassword = (
      <React.Fragment>
        <div>Your Password has been reset!</div>
        <Link to="/">Back to Login</Link>
      </React.Fragment>
    );
  } else {
    RenderResetPassword = (
      <React.Fragment>
        <div className="accountActivation__container__description">
          Enter your new password below and press reset. Then login with your
          new password.
        </div>

        <div className="resetPassword__inputs__container">
          <div className="resetPassword__error">{error}</div>

          <StyledInput
            id="password"
            value={password}
            onChange={onChange}
            disableUnderline
            placeholder={"Password"}
            type="password"
          />

          <StyledInput
            id="confirmPassword"
            value={confirmPassword}
            onChange={onChange}
            disableUnderline
            placeholder={"Confirm Password"}
            type="password"
          />
        </div>

        <Button
          disabled={busy}
          onClick={onSubmit}
          size="sm"
          text="Reset"
          type="curved"
        />
      </React.Fragment>
    );
  }

  return (
    <div className="resetPassword">
      <div className="resetPassword__container">
        <span className="resetPassword__container__header">
          Set a new password
        </span>
        {RenderResetPassword}
      </div>
    </div>
  );
};

const StyledInput = withStyles({
  root: {
    width: "350px",
    fontSize: "14px",
    margin: "4px 0px",
    padding: "8px 16px",
    border: `1px solid ${scss.immerseGreyLight}`
  },
  disabled: {
    color: "black"
  }
})(Input);

export default ResetPassword;
