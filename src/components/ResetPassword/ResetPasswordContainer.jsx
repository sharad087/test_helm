import React, { Component } from "react";
import { connect } from "react-redux";
import ResetPassword from "./ResetPassword";
import { resetPassword } from "../../ducks/resetPassword/actions";
import {
  selectResetPasswordBusy,
  selectResetPasswordReset,
  selectResetPasswordError
} from "../../ducks/resetPassword/selectors";

class ResetPasswordContainer extends Component {
  constructor(props) {
    super(props);

    this.accessToken = this.props.match.params.accessToken;

    this.state = {
      password: "",
      confirmPassword: "",
      error: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.validateInputs = this.validateInputs.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  validateInputs(){
    const { password, confirmPassword } = this.state;
    let error = "";

    if(!password || !confirmPassword) {
      error = "Passwords must not be empty";
    } else if (password !== confirmPassword){
      error = "Password must match the confirm password";
    }

    return error;
  }

  handleSubmit() {
    const { password } = this.state;
    const errors = this.validateInputs();

    this.setState({error: errors});

    if (!errors) {
      this.props.resetPassword(password, this.accessToken);
    }
  }

  render() {
    const { password, confirmPassword, error } = this.state;
    const { busy, reset, resetPasswordError } = this.props;

    const errors = error || resetPasswordError;

    return (
      <ResetPassword
        password={password}
        confirmPassword={confirmPassword}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
        busy={busy}
        reset={reset}
        error={errors}
      />
    );
  }
}

const mapStateToProps = state => ({
  busy: selectResetPasswordBusy(state),
  reset: selectResetPasswordReset(state),
  resetPasswordError: selectResetPasswordError(state)
});

const mapDispatchToProps = dispatch => ({
  resetPassword: (accessToken, password) =>
    dispatch(resetPassword(accessToken, password))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPasswordContainer);
