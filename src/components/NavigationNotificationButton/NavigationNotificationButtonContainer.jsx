import React, { Component } from "react";
import { connect } from "react-redux";

import { getNotification, readNotification } from '../../ducks/notification/actions';
import {
  selectNotificationBusy,
  selectNotificationLoaded,
  selectNotificationError,
  selectNotificationData,
} from '../../ducks/notification/selectors';

import NavigationNotificationButton from "./NavigationNotificationButton";

class NavigationNotificationButtonContainer extends Component {
  constructor(props) {
    super(props);
    console.log('notifciation constructor');

    this.state = {
      openDropdown: false,
      anchorDropdownEl: null,
      notificationCount: 0,
    };

    this.handleToggleDropdown = this.handleToggleDropdown.bind(this);
    this.handleCloseDropdown = this.handleCloseDropdown.bind(this);
    this.handleReadNotification = this.handleReadNotification.bind(this);
  }

  handleToggleDropdown(event) {
    if(!this.state.openDropdown){
      this.props.getNotification({order: 'descending'});
    }


    const { currentTarget } = event;
    this.setState(prevState => ({
      anchorDropdownEl: currentTarget,
      openDropdown: !prevState.openDropdown
    }));
  }

  handleCloseDropdown() {
    this.setState({ anchorDropdownEl: null, openDropdown: false });
  }

  handleReadNotification(id, read){
    if(read){
      console.log('Already read, nothing happens for now');
    }else{
      this.props.readNotification(id);
    }
  }

  render() {
    const {
      openDropdown,
      anchorDropdownEl,
      notificationCount,
    } = this.state;

    const { notifications, busy, loaded, error } = this.props;

    return (
      <NavigationNotificationButton
        open={openDropdown}
        onToggleDropdown={this.handleToggleDropdown}
        onClose={this.handleCloseDropdown}
        onReadNotification={this.handleReadNotification}
        anchorDropdownEl={anchorDropdownEl}
        notificationCount={notificationCount}
        notifications={notifications}
        busy={busy}
        loaded={loaded}
        error={error}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    busy: selectNotificationBusy(state),
    loaded: selectNotificationLoaded(state),
    error: selectNotificationError(state),
    notifications: selectNotificationData(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getNotification: query => dispatch(getNotification(query)),
    readNotification: id => dispatch(readNotification(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavigationNotificationButtonContainer);
