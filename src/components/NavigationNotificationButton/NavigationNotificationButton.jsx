import React from "react";
import ReactSVG from "react-svg";

import { LoadingWheel, NotificationListItem } from "../_shared";

import MenuList from "@material-ui/core/MenuList";
import Popper from "@material-ui/core/Popper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

const NavigationNotificationButton = ({
  open,
  onToggleDropdown,
  onClose,
  onReadNotification,
  anchorDropdownEl,
  notificationCount,
  notifications,
  busy,
  loaded,
  error
}) => {
  let renderedNotifications;

  if (loaded && !busy && notifications.length > 0) {
    //If successfully loaded
    renderedNotifications = notifications.map(
      ({ _id, message, createdAt, read, thumbnail }, index) => {
        return (
          <React.Fragment>
            {index !== 0 && (
              <div className="navigationNotificationDropdown__divider" />
            )}

            <NotificationListItem
              key={_id}
              onClick={() => onReadNotification(_id, read)}
              message={message}
              time={createdAt}
              read={read}
              profileImage={thumbnail}
            />
          </React.Fragment>
        );
      }
    );
  } else if (loaded && !busy && notifications.length === 0) {
    //Loaded succesfully but no notifications
    renderedNotifications = <div>No notifications :(</div>;
  } else if (!loaded && busy) {
    //If loading to get notifications
    renderedNotifications = (
      <LoadingWheel size={50} loadingMessage="Loading Notifications" />
    );
  } else {
    //Else (assume to be an error)
    renderedNotifications = <div>Error :(</div>;
  }

  return (
    <div className="navigationNotificationButton">
      <ClickAwayListener onClickAway={onClose}>
        <div>
          <div
            className={"navigationNotificationButton__button"}
            onClick={onToggleDropdown}
          >
            <ReactSVG
              svgClassName={"navigationNotificationButton__button__svg"}
              src={require("../../images/notification-bell.svg")}
            />
            <div className={"navigationNotificationButton__button__bubble"}>
              <p
                className={
                  "navigationNotificationButton__button__bubble__count"
                }
              >
                {notificationCount}
              </p>
            </div>
          </div>

          <Popper
            className="navigationNotificationDropdown"
            open={open}
            anchorEl={anchorDropdownEl}
            disablePortal
            placement="bottom-end"
            modifiers={{
              flip: {
                enabled: false
              },
              preventOverflow: {
                enabled: false,
                boundariesElement: "scrollParent"
              }
            }}
          >
            <MenuList disablePadding>
              <div className="navigationNotificationDropdown__header">
                Notifications
              </div>
              <div className="navigationNotificationDropdown__notifications">
                {renderedNotifications}
              </div>
            </MenuList>
          </Popper>
        </div>
      </ClickAwayListener>
    </div>
  );
};

NavigationNotificationButton.defaultProps = {
  notificationCount: 0,
  notifications: []
};

export default NavigationNotificationButton;
