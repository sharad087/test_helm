import React from 'react';
import MaterialButton from '@material-ui/core/Button';

const Button = (props) => (
    <MaterialButton className="button" {...props} style={{textTransform: 'none'}}  >
        {props.children}
    </MaterialButton>
)

export default Button;