import React from 'react';
import moment from 'moment';
import ReactSVG from 'react-svg';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker as MUIDatePicker } from 'material-ui-pickers';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';

import scss from '../../../scss/_common.scss';

class DatePicker extends React.Component {
  handleDateChange = date => {
    this.props.handleDateChange(date);
  };

  render() {
    const { start, end, value, startAdornment, endAdornment } = this.props;

    let selectedBackgroundColor = scss.immerseBluePrimary;
    if (start) {
      selectedBackgroundColor = scss.immerseSuccess;
    } else if (end) {
      selectedBackgroundColor = scss.immerseError;
    }

    const immerseTheme = createMuiTheme({
      overrides: {
        MuiFormControl: {
          root: {
            width: '100%'
          }
        },
        MuiPickersToolbar: {
          toolbar: {
            backgroundColor: scss.immerseBluePrimary
          }
        },
        MuiPickersDay: {
          selected: {
            backgroundColor: selectedBackgroundColor,
            '&:hover': {
              backgroundColor: selectedBackgroundColor
            }
          },
          current: {
            color: 'black',
            fontWeight: 'normal'
          }
        },
        MuiPickersModal: {
          dialogAction: {
            color: scss.immerseBluePrimary
          }
        }
      }
    });

    return (
      <div className="date-picker">
        <MuiThemeProvider theme={immerseTheme}>
          <MuiPickersUtilsProvider utils={MomentUtils} moment={moment}>
            <MUIDatePicker
              format={'D MMM YYYY'}
              value={value}
              emptyLabel=""
              onChange={this.handleDateChange}
              autoOk={true}
              animateYearScrolling={false}
              disableFuture={true}
              InputProps={{
                disableUnderline: true,
                startAdornment,
                endAdornment
              }}
              leftArrowIcon={
                <ReactSVG src={require('../../../images/left-arrow.svg')} />
              }
              rightArrowIcon={
                <ReactSVG src={require('../../../images/right-arrow.svg')} />
              }
            />
          </MuiPickersUtilsProvider>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default DatePicker;
