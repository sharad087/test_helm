import React from "react";
import PropTypes from "prop-types";
import moment from "moment";

import { ProfileIcon } from "../";

const NotificationListItem = props => {
  const { profileImage, message, time, read, onClick } = props;
  const formattedTime = time ? moment(time).from() : undefined;

  return (
    <div className= {read ? "notificationListItem--read" : "notificationListItem"} onClick={onClick}> 
      <ProfileIcon src={profileImage} />
      <div className="notificationListItem__message">
        <div className="notificationListItem__message__description">
          {message}
        </div>
        {formattedTime !== "Invalid date" && (
          <div className="notificationListItem__message__time">
            {formattedTime}
          </div>
        )}
      </div>
    </div>
  );
};

NotificationListItem.defaultProps = {
  message: "",
  read: false,
};

NotificationListItem.propTypes = {
  message: PropTypes.string,
  read: PropTypes.bool,
};

export default NotificationListItem;
