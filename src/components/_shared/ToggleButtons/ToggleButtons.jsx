import React from "react";
import PropTypes from "prop-types";
import scss from "../../../scss/_common.scss";

import { withStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";

const styles = {
  root: {
    flex: 1,
    color: scss.immerseGreyDark,
    "&:hover": {
      borderRadius: '100px',
    },

    "&$selected": {
      color: 'white',
      backgroundColor: scss.immerseBluePrimary,
      borderRadius: '100px',
      "&:hover": {
        backgroundColor: scss.immerseBluePrimary,
      }
    }
  },
  selected: {
    //Keep this and keep it empty in order to work. Edit "&$selected" instead.
  },
  label: {
    textTransform: "capitalize",
    fontSize: '14px',
  },
  groupRoot: {
    backgroundColor: scss.immerseBlueOpacity,
    borderRadius: "100px",
    display: 'flex',
    boxShadow: 'none',
  }
};

const ToggleButtons = ({ classes, selected, values, onChange }) => (
  <div>
    <ToggleButtonGroup
      classes={{ root: classes.groupRoot }}
      value={selected}
      exclusive
      onChange={onChange}
    >
      {values.map(value => (
        <ToggleButton
          classes={{
            root: classes.root,
            selected: classes.selected,
            label: classes.label
          }}
          selected={selected === value}
          key={value}
          value={value}
        >
          {value}
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  </div>
);

ToggleButtons.defaultProps = {
  values: []
};

ToggleButtons.propTypes = {
  values: PropTypes.arrayOf(PropTypes.string).isRequired,
  selected: PropTypes.string,
  onChange: PropTypes.func
};

export default withStyles(styles)(ToggleButtons);
