import React from 'react';

const Card = props => {
  const shadow = props.shadow ? 'card--shadow' : '';
  const clickCursor = props.onClick ? 'card--onClick' : '';
  let padding;

  switch(props.padding) {
    case('none'):
      padding = '';
      break;
    case('small'):
      padding = 'card--small';
      break;
    case('medium'):
      padding = 'card--medium';
      break;
    case('large'):
      padding = 'card--large';
      break;
    default: 
      padding = 'card--small';
  } 

  return (
    <div className={`card ${padding} ${shadow} ${clickCursor}`} onClick={props.onClick}>
      {props.children}
    </div>
  );
}

Card.defaultProps = {
  padding: 'small',
  shadow: true,
}

export default Card;