import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const LoadingWheel = ({size, loadingMessage}) => {
  return (
    <div className="loadingWheel">
      <CircularProgress color="primary" size={size} thickness={2}/>
      <div className="loadingWheel__message">{loadingMessage}</div>
    </div>
  )
}

LoadingWheel.defaultProps = {
  size: 100,
  loadingMessage: "Loading"
}

export default LoadingWheel;