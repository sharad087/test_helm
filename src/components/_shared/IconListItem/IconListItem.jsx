import React from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';

const IconListItem = ({src, text, subtext, bold}) => (
    <div className="iconListItem">
        <ReactSVG src={src}/>
        <div className="iconListItem__textContainer">
            {bold ? <strong>{text}</strong> : <div>{text}</div>}
            {subtext}
        </div>
    </div>
)

IconListItem.defaultProps = {
    src: require('../../../images/default_profile.svg'),
    text: "",
    subtext: "",
    bold: false,
}

IconListItem.propTypes = {
    text: PropTypes.string.isRequired
}

export default IconListItem;