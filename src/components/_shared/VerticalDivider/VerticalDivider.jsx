import React from 'react';

const VerticalDivider = (props) => (
    <div className={`verticalDivider ${props.className}`}></div>
)

VerticalDivider.defaultProps = {
    width: 1,
}

export default VerticalDivider;