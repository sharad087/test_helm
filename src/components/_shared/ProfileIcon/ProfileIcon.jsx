import React, { Component } from "react";
import ReactSVG from "react-svg";
import scss from "../../../scss/_common.scss";
import { extractInitials } from "../../../utils";
import MUIAvatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  extraSmall: {
    width: scss.iconExtraSmall,
    height: scss.iconExtraSmall
  },
  small: {
    width: scss.iconSmall,
    height: scss.iconSmall
  },
  medium: {
    width: scss.iconMedium,
    height: scss.iconMedium
  },
  large: {
    width: scss.iconLarge,
    height: scss.iconLarge
  }
};

class ProfileIcon extends Component {
  constructor(props) {
    super(props);
    this.sizeClassName = this.props.classes.small;
    this.defaultName = extractInitials(this.props.name);
    this.defaultProfileImage = require("../../../images/default_profile.svg");

    this.state = {
      profileImage: this.props.src,
      name: this.defaultName
    };

    this.handleInvalidImageError = this.handleInvalidImageError.bind(this);
    this.handleOnLoadImage = this.handleOnLoadImage.bind(this);
  }

  componentWillMount() {
    switch (this.props.size) {
      case "large":
        this.sizeClassName = this.props.classes.large;
        break;
      case "medium":
        this.sizeClassName = this.props.classes.medium;
        break;
      case "extraSmall":
        this.sizeClassName = this.props.classes.extraSmall;
        break;
      default:
        this.sizeClassName = this.props.classes.small;
        break;
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.name !== this.props.name) {
      const updatedName = extractInitials(this.props.name);
      this.setState({ name: updatedName });
    }
    if (prevProps.src !== this.props.src) {
      this.setState({ profileImage: this.props.src });
    }
  }

  handleInvalidImageError() {
    this.setState({ profileImage: undefined });
  }

  handleOnLoadImage() {
    if (this.props.src) {
      this.setState({ profileImage: this.props.src });
    } else {
      this.setState({ profileImage: undefined });
    }
  }

  render() {
    const { profileImage, name } = this.state;

    const ImageErrorChecker = (
      <img
        className="imageErrorChecker"
        alt=""
        src={this.props.src ? this.props.src : ""}
        onError={this.handleInvalidImageError}
        onLoad={this.handleOnLoadImage}
      />
    );

    return (
      <React.Fragment>
        {ImageErrorChecker}
        <MUIAvatar
          alt=""
          src={profileImage}
          className={this.sizeClassName}
          tabIndex="0"
        >
          {name ? (
            name
          ) : (
            <ReactSVG
              src={this.defaultProfileImage}
              svgClassName="defaultProfileImage"
            />
          )}
        </MUIAvatar>
      </React.Fragment>
    );
  }
}

ProfileIcon.defaultProps = {
  size: "small",
  name: "",
  src: undefined
};

export default withStyles(styles)(ProfileIcon);
