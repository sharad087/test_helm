import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MUISnackbar from '@material-ui/core/Snackbar';

import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseIcon from '@material-ui/icons/Close';

import scss from '../../../scss/_common.scss';

const styles = theme => ({
  snackbar: {
    bottom: '30px'
  },
  error: {
    backgroundColor: scss.immerseError
  },
  success: {
    backgroundColor: scss.immerseSuccess
  },
  content: {
    display: 'flex',
    alignItems: 'center'
  },
  icon: {
    fontSize: 24
  },
  message: {
    marginLeft: scss.gMargin,
    fontSize: 14
  },
  closeIcon: {
    fontSize: 24,
    transition: '0.3s',

    '&:hover': {
      cursor: 'pointer',
      color: scss.immerseGreyLight
    }
  }
});

class Snackbar extends React.Component {
  handleClose = () => {
    this.props.onClose();
  };

  render() {
    const {
      vertical,
      horizontal,
      open,
      message,
      classes,
      type,
      autoHideDuration
    } = this.props;

    if (type === 'error') {
      const SnackbarContent = (
        <div className={classes.content}>
          <ErrorIcon className={classes.icon} />
          <span className={classes.message}>{message}</span>
        </div>
      );

      return (
        <MUISnackbar
          anchorOrigin={{ vertical, horizontal }}
          className={classes.snackbar}
          open={open}
          onClose={this.handleClose}
          ContentProps={{
            classes: {
              root: classes.error
            }
          }}
          message={SnackbarContent}
          action={
            <CloseIcon
              className={classes.closeIcon}
              onClick={this.handleClose}
            />
          }
          variant='error'
          autoHideDuration={autoHideDuration}
          ClickAwayListenerProps={{ onClickAway: () => {} }}
        />
      );
    } else {
      const SnackbarContent = (
        <div className={classes.content}>
          <CheckCircleIcon className={classes.icon} />
          <span className={classes.message}>{message}</span>
        </div>
      );

      return (
        <MUISnackbar
          anchorOrigin={{ vertical, horizontal }}
          className={classes.snackbar}
          open={open}
          onClose={this.handleClose}
          ContentProps={{
            classes: {
              root: classes.success
            }
          }}
          message={SnackbarContent}
          action={
            <CloseIcon
              className={classes.closeIcon}
              onClick={this.handleClose}
            />
          }
          variant='success'
        />
      );
    }
  }
}

export default withStyles(styles)(Snackbar);
