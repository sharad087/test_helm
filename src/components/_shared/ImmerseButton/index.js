import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core';

import scss from '../../../scss/_common.scss';

const ImmerseButton = props => {
  const {
    text,
    size,
    disabled,
    onClick,
    type,
    startAdornment,
    endAdornment
  } = props;

  let height = '60px';
  let width = '320px';
  let fontSize = '23px';
  let borderRadius = '5px';
  let border = 'none';
  let color = 'white';

  let backgroundcolor = scss.immerseBluePrimary;

  switch (size) {
    case 'sm':
      height = '40px';
      width = '100px';
      fontSize = '14px';
      borderRadius = '2px';
      break;

    case 'md':
      height = '40px';
      width = '140px';
      fontSize = '15px';
      borderRadius = '3px';
      break;

    case 'lg':
      height = '50px';
      width = '320px';
      fontSize = '15px';
      borderRadius = '4px';
      break;
    case 'xl':
      height = '50px';
      width = '400px';
      fontSize = '15px';
      borderRadius = '4px';
      break;

    default:
      break;
  }

  if (type === 'secondary') {
    backgroundcolor = scss.immerseSuccess;
  }

  if (type === 'curved') {
    borderRadius = '25px';
  }

  if (type === 'curved-transparent') {
    borderRadius = '25px';
    backgroundcolor = 'white';
    border = `1px solid ${scss.immerseBluePrimary}`;
    color = scss.immerseBluePrimary;
  }

  const StyledButton = withStyles({
    root: {
      height,
      width,
      backgroundColor: backgroundcolor,
      '&:hover': {
        backgroundColor: backgroundcolor,
      },
      '&:active': {
        backgroundColor: backgroundcolor,
      },
      boxShadow: '0 18px 36px 0px #E1EAFC',
      borderRadius,
      border
    },
    label: {
      color,
      textTransform: 'initial',
      fontSize,
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center'
    }
  })(Button);

  const DisabledStyledButton = withStyles({
    root: {
      height,
      width,
      backgroundColor: scss.immerseGreyLight,
      boxShadow: '0 18px 36px 0px #E1EAFC',
      borderRadius
    },
    label: {
      color: '#fff',
      textTransform: 'initial',
      fontSize
    }
  })(Button);

  if (disabled) {
    return <DisabledStyledButton>{text}</DisabledStyledButton>;
  } else {
    return (
      <StyledButton onClick={onClick} variant="contained">
        {startAdornment}
        {text}
        {endAdornment}
      </StyledButton>
    );
  }
};

ImmerseButton.defaultProps = {
  text: 'Button',
  size: 'lg',
  disabled: false,
  onClick: () => console.log('Button Clicked...'),
  backgroundcolor: scss.immerseBluePrimary
};

ImmerseButton.propTypes = {
  text: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default ImmerseButton;
