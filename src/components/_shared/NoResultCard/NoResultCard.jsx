import React from "react";

const NoResultCard = props => {
  return (
    <div className="noResultCard">
      <div className="noResultCard__interiorBorder">
        <div className={props.className}>
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default NoResultCard;
