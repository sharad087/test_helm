import React from "react";

import Modal from "@material-ui/core/Modal";

import Button from "../_shared/ImmerseButton";
import IconListItem from "../_shared/IconListItem/IconListItem";

import CloseIcon from "@material-ui/icons/Close";

const ClassPurchaseModal = ({
  open,
  onClose,
  course,
  onSubmit,
  busy,
  error
}) => {
  const {
    title,
    subtitle,
    description,
    duration,
    startEnd,
    classSize,
    lesson,
    location,
    organization
  } = course;

  return (
    <Modal
      className="classPurchaseModalContainer"
      open={open}
      onClose={onClose}
    >
      <div className="classPurchaseModal">
        <CloseIcon
          className="classPurchaseModal__closeIcon"
          onClick={onClose}
        />

        <div className="classPurchaseModal__infoContainer">
          <div className="classPurchaseModal__infoContainer__header">
            <div className="classPurchaseModal__infoContainer__header__subtitle">
              {subtitle}
            </div>
            <h2 className="classPurchaseModal__infoContainer__header__title">
              {title}
            </h2>
          </div>
          <div className="classPurchaseModal__infoContainer__description">
            {description}
          </div>
          <div className="classPurchaseModal__infoContainer__details">
            <div className="classPurchaseModal__infoContainer__details__row">
              <IconListItem
                text={duration}
                src={require("../../images/time-clock.svg")}
              />
              <IconListItem
                text={lesson}
                src={require("../../images/teach-blue.svg")}
              />
            </div>
            <div className="classPurchaseModal__infoContainer__details__row">
              <IconListItem
                text={startEnd}
                src={require("../../images/check-in.svg")}
              />
              <IconListItem
                text={location}
                src={require("../../images/pin.svg")}
              />
            </div>
            <div className="classPurchaseModal__infoContainer__details__row">
              <IconListItem
                text={classSize}
                src={require("../../images/students.svg")}
              />
              <IconListItem
                text={organization}
                src={require("../../images/organization.svg")}
              />
            </div>
          </div>
          <div className="classPurchaseModal__enrollButton">
            <Button
              text="Request Enrollment"
              type="curved"
              size="lg"
              onClick={onSubmit}
              disabled={busy}
            />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ClassPurchaseModal;
