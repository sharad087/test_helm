import React, { Component } from "react";
import { connect } from "react-redux";
import * as utils from "../../utils";
import ClassPurchaseModal from "./ClassPurchaseModal";
import { requestEnrollment } from "../../ducks/classes/actions";
import {
  selectClassesIsRequestingEnrollment,
  selectClassesRequestEnrollmentError
} from "../../ducks/classes/selectors";

class ClassPurchaseModalContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courseData: {
        id: "",
        title: "",
        subtitle: "",
        description: "",
        duration: "",
        startEnd: "",
        classSize: "",
        lesson: "",
        location: "",
        organization: "",
      }
    };

    this.onRequestEnrollment = this.onRequestEnrollment.bind(this);
  }

  onRequestEnrollment() {
    if (this.state.courseData.id) {
      this.props.requestEnrollment(this.state.courseData.id).then(() => {
        if (!this.props.error) {
          this.props.onClose();
        }
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.open && !prevProps.open) {
      const {
        _id,
        title,
        subtitle,
        description,
        daysOfWeek,
        time,
        lessonCount,
        startDate,
        endDate,
        duration,
        location,
        currentSize,
        maxSize,
        organization
      } = this.props.course;

      const durationString = utils.durationToString(time, duration, daysOfWeek);
      const lessonString = utils.lessonToString(lessonCount);
      const startEndString = utils.startEndToString(startDate, endDate);
      const locationString = location || "No Location Available";
      const classSizeString = utils.classSizeToString(
        currentSize,
        maxSize
      );
      const organizationString = organization.name || "No Organization Available";

      this.setState({
        courseData: {
          id: _id,
          title: title,
          subtitle: subtitle,
          description: description,
          duration: durationString,
          startEnd: startEndString,
          classSize: classSizeString,
          lesson: lessonString,
          location: locationString,
          organization: organizationString
        }
      });
    }
  }

  render() {
    const { open, onClose } = this.props;
    const { courseData } = this.state;
    return (
      <ClassPurchaseModal
        open={open}
        onClose={onClose}
        course={courseData}
        onSubmit={this.onRequestEnrollment}
        busy={this.props.busy}
        error={this.props.error}
      />
    );
  }
}

ClassPurchaseModalContainer.defaultProps = {
  open: false,
  course: {}
};

const mapStateToProps = state => {
  return {
    busy: selectClassesIsRequestingEnrollment(state),
    error: selectClassesRequestEnrollmentError(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    requestEnrollment: classId => dispatch(requestEnrollment(classId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassPurchaseModalContainer);
