import React from "react";
import { ToggleButtons } from "../_shared";

const StudentMyClasses = props => {
  return (
    <div className="studentMyClasses">
      <div className="studentMyClasses__header">
        <h2 className="studentMyClasses__header__title">My Courses</h2>
        <div className="studentMyClasses__toggleButtons">
          <ToggleButtons
            selected={props.toggleValue}
            values={props.toggleValues}
            onChange={props.onToggleChange}
          />
        </div>
      </div>

      {props.children}
    </div>
  );
};

export default StudentMyClasses;
