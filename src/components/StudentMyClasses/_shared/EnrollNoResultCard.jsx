import React from "react";
import ReactSVG from "react-svg";

import { NoResultCard, ImmerseButton } from "../../_shared";

const EnrollNoResultCard = props => {
  const { title, subtitle, onClick } = props;

  return (
    <NoResultCard className="studentMyClassesNoResultCard">
      <ReactSVG src={require("../../../images/virtual-reality-1.svg")} />

      <div className="studentMyClassesNoResultCard__info">
        <h3>{title}</h3>
        <div className="studentMyClassesNoResultCard__subtitle">{subtitle}</div>
      </div>

      <ImmerseButton onClick={onClick} text="Book a Course" type="curved" size="lg" />
    </NoResultCard>
  );
};

EnrollNoResultCard.defaultProps = {
  title: "",
  subtitle: ""
};

export default EnrollNoResultCard;
