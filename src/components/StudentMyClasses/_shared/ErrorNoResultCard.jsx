import React from "react";

import { NoResultCard } from "../../_shared";

const ErrorNoResultCard = props => {
  const { title, subtitle } = props;

  return (
    <NoResultCard className="studentMyClassesNoResultCard">
      <div className="studentMyClassesNoResultCard__info">
        <h3>{title}</h3>
        <div className="studentMyClassesNoResultCard__subtitle">{subtitle}</div>
      </div>
    </NoResultCard>
  );
};

ErrorNoResultCard.defaultProps = {
  title: "",
  subtitle: ""
};

export default ErrorNoResultCard;