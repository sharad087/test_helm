import React from "react";

import { NoResultCard, ImmerseButton } from "../../_shared";

const CompletedNoResultCard = props => {
  const { title, subtitle, onClick } = props;

  return (
    <NoResultCard className="studentMyClassesNoResultCard">
      <div className="studentMyClassesNoResultCard__info">
        <h3>{title}</h3>
        <div className="studentMyClassesNoResultCard__subtitle">{subtitle}</div>
      </div>

      <ImmerseButton onClick={onClick} text="View active course" type="curved" size="lg" />
    </NoResultCard>
  );
};

CompletedNoResultCard.defaultProps = {
  title: "",
  subtitle: ""
};

export default CompletedNoResultCard;