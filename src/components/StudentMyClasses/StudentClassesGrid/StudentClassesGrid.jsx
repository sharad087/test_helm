import React from "react";
import StudentMyClassCard from "./StudentMyClassCard";

const StudentClassesGrid = ({ courses, onCourseClick }) => (
  <div className="studentClassesGrid">
    {courses.map((course, index) => {
      return (
        <div className="studentMyClassCardContainer" key={index}>
          <StudentMyClassCard course={course} onCourseClick={() => onCourseClick(course.id)}/>
        </div>
      );
    })}
  </div>
);

StudentClassesGrid.defaultProps = {
  courses: []
};

export default StudentClassesGrid;
