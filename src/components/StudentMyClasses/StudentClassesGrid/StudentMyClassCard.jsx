import React from "react";
import ReactSVG from "react-svg";
import * as utils from "../../../utils";

import Tooltip from "@material-ui/core/Tooltip";

import IconListItem from "../../_shared/IconListItem/IconListItem";
import ProfileIcon from "../../_shared/ProfileIcon/ProfileIcon";
import { Card } from "../../_shared";

const StudentMyClassCard = ({ course, onCourseClick }) => {
  const {
    title,
    subtitle,
    daysOfWeek,
    time,
    lessonCount,
    startDate,
    endDate,
    duration,
    currentSize,
    maxSize,
    organization,
    teacher
  } = course;

  let organizationName;
  let teacherName;
  let teacherProfileImage = undefined;

  if (teacher) {
    const { name, profileImage } = course.teacher;
    teacherName = name;
    teacherProfileImage = profileImage;
  }

  if (organization) {
    const { name } = course.organization;
    organizationName = name;
  }

  const durationString = utils.durationToString(time, duration, daysOfWeek);
  const lessonString = utils.lessonToString(lessonCount);
  const startEndString = utils.startEndToString(startDate, endDate);
  const classSizeString = utils.classSizeToString(currentSize, maxSize);
  const organizationString = organizationName || "No Organization Available";

  return (
    <Card padding="large" onClick={onCourseClick}>
      <div className="studentMyClassCard">
        <div className="studentMyClassCard__header">
          <div>
            <div className="studentMyClassCard__header__subtitle">
              {subtitle}
            </div>
            <h2 className="studentMyClassCard__header__title">{title}</h2>
          </div>
        </div>

        <div className="studentMyClassCard__infoContainer">
          <IconListItem
            text={durationString}
            src={require("../../../images/time-clock.svg")}
          />
          <IconListItem
            text={lessonString}
            src={require("../../../images/teach-blue.svg")}
          />
          <IconListItem
            text={startEndString}
            src={require("../../../images/check-in.svg")}
          />
          <IconListItem
            text={classSizeString}
            src={require("../../../images/students.svg")}
          />
          <IconListItem
            text={organizationString}
            src={require("../../../images/organization.svg")}
          />
        </div>

        <div className="studentMyClassCard__teacherContainer">
          <Tooltip title={teacherName} placement="top">
            <div>
              <ProfileIcon
                size="small"
                name={teacherName}
                src={teacherProfileImage}
              />
            </div>
          </Tooltip>
        </div>
      </div>
      <ReactSVG
        src={require("../../../images/waves-409.svg")}
        svgClassName="studentMyClassCard__waves"
      />
    </Card>
  );
};

StudentMyClassCard.defaultProps = {
  course: {
    teacher: {
      name: "Teacher"
    }
  }
};

export default StudentMyClassCard;
