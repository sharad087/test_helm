import React, { Component } from "react";
import { connect } from "react-redux";
import { getMyClasses, getLobbyUrl } from "../../ducks/classes/actions";
import {
  selectClassesMyClasses,
  selectClassesBusy,
  selectClassesError,
  selectClassesLoaded,
  selectClassesMyClassesNoMore,
  selectClassesMyClassesQuery,
  selectClassesIsMyClassesInitialzied,
  selectClassesMyClassesSkip,
  selectClassesLobbyUrl,
  selectClassesGotLobbyUrl,
  selectClassesBusyLobbyUrl
} from "../../ducks/classes/selectors";

import StudentMyClass from "./StudentMyClasses";
import StudentClassesGrid from "./StudentClassesGrid/StudentClassesGrid";
import { LoadingWheel } from "../_shared";
import EnrollNoResultCard from "./_shared/EnrollNoResultCard";
import CompletedNoResultCard from "./_shared/CompletedNoResultCard";
import ErrorNoResultCard from "./_shared/ErrorNoResultCard";
import { selectAccountProfile } from "../../ducks/account/selectors";

const immerseLobbyURL = process.env.REACT_APP_IMMERSE_LOBBY_URL || "";
const toggleValues = ["all", "active", "completed"];

class StudentMyClassesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleValue: this.props.query.status || toggleValues[0],
    };
    this.getCourses = this.getCourses.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleCourseClick = this.handleCourseClick.bind(this);
    this.renderClassesGrid = this.renderClassesGrid.bind(this);
  }

  componentDidMount() {
    //If there is no data already loaded (switching from tabs) or on initial page load
    if (!this.props.initialized) {
      this.getCourses();
    }

    window.addEventListener("scroll", this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll, false);
  }

  onScroll() {
    // If the page is scrolled to the bottom, not busy (from current loading)
    // more classes, and there is still more to load.

    //The state noMore allows for the page to try loading more if it switches
    //page. This is in case a new class is added while browsing another page.
    if (
      !this.props.busy &&
      !this.props.noMore &&
      !this.props.error &&
      window.innerHeight + window.scrollY >=
        document.getElementById("root").offsetHeight - 1
    ) {
      const query = this.state.toggleValue === "all" ? undefined : { status: this.state.toggleValue }

      this.getCourses(6, this.props.classesData.length, query)
    }
  }

  getCourses(limit = 6, skip = 0, query = {}) {
    const adjustedQuery = { ...query, students: this.props.profile.id };
    this.props.getMyClasses(limit, skip, adjustedQuery);
  }

  handleToggle(event, toggle) {
    //If the toggle option is not itself or nothing at all
    if (toggle) {
      this.setState({ toggleValue: toggle }, () => {
        let query = { status: toggle };
      
        if(toggle === 'all') {
          query = undefined;
        }else if (toggle === 'completed') {
          query = { completed: true};
        }

        this.getCourses(6, 0, query)
      });
    }
  }

  handleCourseClick(courseId) {
    if(!this.props.busyLobbyUrl){
      this.props.getLobbyUrl(courseId)
      .then(() => {
        if(this.props.gotLobbyUrl){
          window.open(this.props.lobbyUrl);
        }
      })
    }
  }

  renderClassesGrid() {
    const { busy, loaded, error, classesData, skip } = this.props;
    const { toggleValue } = this.state;

    if (busy && skip === 0) {
      return <LoadingWheel />;
    } else if (loaded && !error && classesData.length === 0) {
      if (toggleValue === "all" || toggleValue === "active") {
        return (
          <EnrollNoResultCard
            onClick={() => this.props.history.push("/dashboard/purchase")}
            title="You haven't enrolled in any courses yet!"
            subtitle='Visit the "Book a Course" tab to enroll'
          />
        );
      } else {
        return (
          <CompletedNoResultCard
            onClick={() => this.handleToggle(null, "active")}
            title="You haven't completed any courses yet!"
            subtitle="Once your first course comes to a close, it will show up here."
          />
        );
      }
    } else if (!error && classesData.length > 0) {
      return <StudentClassesGrid courses={classesData} onCourseClick={this.handleCourseClick}/>;
    } else {
      return <ErrorNoResultCard title="Error :(" subtitle="Please try again" />;
    }
  }

  render() {
    const { toggleValue } = this.state;
    const ClassesGrid = this.renderClassesGrid();

    return (
      <StudentMyClass
        toggleValue={toggleValue}
        toggleValues={toggleValues}
        onToggleChange={this.handleToggle}
      >
        {ClassesGrid}
      </StudentMyClass>
    );
  }
}

const mapStateToProps = state => {
  return {
    classesData: selectClassesMyClasses(state),
    error: selectClassesError(state),
    busy: selectClassesBusy(state),
    loaded: selectClassesLoaded(state),
    noMore: selectClassesMyClassesNoMore(state),
    query: selectClassesMyClassesQuery(state),
    initialized: selectClassesIsMyClassesInitialzied(state),
    skip: selectClassesMyClassesSkip(state),
    profile: selectAccountProfile(state),
    lobbyUrl: selectClassesLobbyUrl(state),
    gotLobbyUrl: selectClassesGotLobbyUrl(state),
    busyLobbyUrl: selectClassesBusyLobbyUrl(state),
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getMyClasses: (limit, skip, query) =>
      dispatch(getMyClasses(limit, skip, query)),
    getLobbyUrl: courseId => dispatch(getLobbyUrl(courseId)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StudentMyClassesContainer);
