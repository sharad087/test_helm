import React from "react";
import ReactSVG from "react-svg";

import { withStyles } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Input from "@material-ui/core/Input";
import Modal from "@material-ui/core/Modal";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";

import Button from "../_shared/Button/Button";

const CreateAccountModal = props => {
  const {
    name,
    email,
    password,
    confirmPassword,
    errors,
    open,
    onClose,
    accountError,
    onChange,
    onCreateAccount
  } = props;

  return (
    <Modal
      className={"create-account-modal"}
      open={open}
      onClose={onClose}
      disableAutoFocus
    >
      <div className={"create-account-container"}>
        <ReactSVG
          className={"close-icon"}
          src={require("../../images/close_icon.svg")}
          onClick={onClose}
        />
        <ReactSVG
          className={"immerse-logo"}
          src={require("../../images/immerse_logo.svg")}
        />

        <div className={"create-account-text"}>
          Create an account with Immerse
        </div>
        <div className={"create-account-input-container"}>
          <StyledPaper>
            <div>{accountError}</div>
            <StyledInput
              id="name"
              value={name}
              onChange={onChange}
              disableUnderline
              placeholder={"Full Name"}
              endAdornment={
                errors.hasOwnProperty("name") && (
                  <InputAdornment position="start">
                    <Tooltip title={errors.name}>
                      <ErrorOutlineIcon color="secondary" />
                    </Tooltip>
                  </InputAdornment>
                )
              }
            />
            <Divider />
            <StyledInput
              id="email"
              value={email}
              onChange={onChange}
              disableUnderline
              placeholder={"Email"}
              endAdornment={
                errors.hasOwnProperty("email") && (
                  <InputAdornment position="start">
                    <Tooltip title={errors.email}>
                      <ErrorOutlineIcon color="secondary" />
                    </Tooltip>
                  </InputAdornment>
                )
              }
            />
            <Divider />
            <StyledInput
              id="password"
              value={password}
              type="password"
              onChange={onChange}
              disableUnderline
              placeholder={"Password"}
              endAdornment={
                errors.hasOwnProperty("password") && (
                  <InputAdornment position="start">
                    <Tooltip title={errors.password}>
                      <ErrorOutlineIcon color="secondary" />
                    </Tooltip>
                  </InputAdornment>
                )
              }
            />
            <Divider />
            <StyledInput
              id="confirmPassword"
              value={confirmPassword}
              type="password"
              onChange={onChange}
              disableUnderline
              placeholder={"Confirm Password"}
              endAdornment={
                errors.hasOwnProperty("confirmPassword") && (
                  <InputAdornment position="start">
                    <Tooltip title={errors.confirmPassword}>
                      <ErrorOutlineIcon color="secondary" />
                    </Tooltip>
                  </InputAdornment>
                )
              }
            />
          </StyledPaper>
        </div>

        <CreateAccountSubmitButton
          onClick={onCreateAccount}
          variant="contained"
          color="primary"
          fullWidth
        >
          Create Account
        </CreateAccountSubmitButton>
      </div>
    </Modal>
  );
};

export default CreateAccountModal;

const StyledInput = withStyles({
  root: {
    width: "100%",
    margin: 8
  }
})(Input);

const StyledPaper = withStyles({
  root: {
    boxShadow: "0 18px 36px 0px #E1EAFC"
  }
})(Paper);

const CreateAccountSubmitButton = withStyles({
  root: {
    textTransform: "capitalize",
    boxShadow: "0 18px 36px 0px #E1EAFC"
  }
})(Button);
