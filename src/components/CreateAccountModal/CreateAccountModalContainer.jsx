import React, { Component } from "react";
import { connect } from "react-redux";

import CreateAccountModal from "./CreateAccountModal";

import { selectAccountError } from "../../ducks/account/selectors";
import { createAccount } from "../../ducks/account/actions";
import { loginAccount } from "../../ducks/authenticate/actions";

class CreateAccountModalContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      password: "",
      confirmPassword: "",

      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.validateCreateAccount = this.validateCreateAccount.bind(this);
    this.handleCreateAccount = this.handleCreateAccount.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  validateCreateAccount() {
    const { name, email, password, confirmPassword } = this.state;
    const errors = {};

    if (!name) {
      errors.name = "Full Name Required";
    }
    if (!email) {
      errors.email = "Email Required";
    }
    if (!password) {
      errors.password = "Password Required";
    } else if (password !== confirmPassword) {
      errors.confirmPassword = "Passwords do not match";
    }

    return errors;
  }

  handleCreateAccount() {
    const { onCreateAccount, onLoginSubmit } = this.props;
    const { name, email, password } = this.state;
    const errors = this.validateCreateAccount();

    if (Object.keys(errors).length === 0) {
      onCreateAccount(email, password, name)
        .then(() => {
          if (!this.props.accountError) {
            onLoginSubmit(email, password);
          }
        });
    } else {
      this.setState({ errors: errors });
    }
  }

  render() {
    const { name, email, password, confirmPassword, errors } = this.state;
    const { open, onClose, accountError } = this.props;

    return (
      <CreateAccountModal
        open={open}
        onClose={onClose}
        name={name}
        email={email}
        password={password}
        confirmPassword={confirmPassword}
        errors={errors}
        accountError={accountError}
        onChange={this.handleChange}
        onCreateAccount={this.handleCreateAccount}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    accountError: selectAccountError(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoginSubmit: (email, password) => dispatch(loginAccount(email, password)),
    onCreateAccount: (email, password, fullName) =>
      dispatch(createAccount(email, password, fullName))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateAccountModalContainer);
