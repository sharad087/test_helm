import React from "react";
import { Link } from "react-router-dom";
import Button from "../_shared/ImmerseButton";

import { withStyles } from "@material-ui/core";
import Input from "@material-ui/core/Input";

import scss from "../../scss/_common.scss";

const AccountActivation = props => {
  const {
    name,
    password,
    confirmPassword,
    onChange,
    onSubmit,
    busy,
    error,
    activated,
  } = props;

  let RenderAccountActivation;

  if (!activated && !busy && error) {
    RenderAccountActivation = (
      <React.Fragment>
        <div>Error :(</div>
        <Link to="/">Back to Login</Link>
      </React.Fragment>
    );
  } else {
    RenderAccountActivation = (
      <React.Fragment>
        <div className="accountActivation__container__description">
          Enter your name and password below to create an account
        </div>

        <div className="accountActivation__inputs__container">
          <div className="accountActivation__error">{error}</div>
          <StyledInput
            id="name"
            value={name}
            onChange={onChange}
            placeholder={"Full Name"}
            disableUnderline
          />

          <StyledInput
            id="password"
            value={password}
            onChange={onChange}
            placeholder={"Password"}
            disableUnderline
            type="password"
          />

          <StyledInput
            id="confirmPassword"
            value={confirmPassword}
            onChange={onChange}
            placeholder={"Confirm Password"}
            disableUnderline
            type="password"
          />
        </div>
        <Button
          onClick={onSubmit}
          disabled={busy}
          text="Create Account"
          type="curved"
          size="md"
        />
      </React.Fragment>
    );
  }

  return (
    <div className="accountActivation">
      <div className="accountActivation__container">
        <span className="accountActivation__container__header">
          Create an Account
        </span>
        {RenderAccountActivation}
      </div>
    </div>
  );
};

const StyledInput = withStyles({
  root: {
    width: "350px",
    fontSize: "14px",
    margin: "4px 0px",
    padding: "8px 16px",
    border: `1px solid ${scss.immerseGreyLight}`
  },
  disabled: {
    color: "black"
  }
})(Input);

export default AccountActivation;
