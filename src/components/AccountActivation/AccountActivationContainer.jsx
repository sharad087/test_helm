import React, { Component } from "react";
import { connect } from "react-redux";
import AccountActivation from "./AccountActivation";
import { refreshAccessToken } from "../../ducks/authenticate/actions";
import { getProfile } from "../../ducks/account/actions";
import { activateAccount } from "../../ducks/activation/actions";
import {
  selectActivationBusy,
  selectActivationError,
  selectActivationHasActivatedAccount
} from "../../ducks/activation/selectors";

class AccountActivationContainer extends Component {
  constructor(props) {
    super(props);

    this.accessToken = this.props.match.params.accessToken;

    this.state = {
      name: "",
      password: "",
      confirmPassword: "",
      error: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.validateAccountInputs = this.validateAccountInputs.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  validateAccountInputs() {
    const { name, password, confirmPassword } = this.state;
    let error = "";

    if (!name && !password) {
      error = "Please enter your name and password";
    } else if (!name) {
      error = "Name must not be empty";
    } else if (!password || !confirmPassword) {
      error = "Passwords must not be empty";
    } else if (password !== confirmPassword) {
      error = "Password must match the confirm password";
    }

    return error;
  }

  handleSubmit() {
    const error = this.validateAccountInputs();

    this.setState({ error: error });
    if (!error) {
      const { name, password } = this.state;

      this.props.activateAccount(name, password, this.accessToken).then(() => {
        if (this.props.isActivated) {
          this.props.refreshAccessToken()
          .then(() => this.props.history.push("/dashboard"));
        }
      });
    }
  }

  render() {
    const { name, password, confirmPassword, error } = this.state;
    const { busy, isActivated, activationError } = this.props;

    const errors = error || activationError;

    return (
      <AccountActivation
        name={name}
        password={password}
        confirmPassword={confirmPassword}
        activated={isActivated}
        busy={busy}
        error={errors}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
      />
    );
  }
}

const mapStateToProps = state => ({
  busy: selectActivationBusy(state),
  activationError: selectActivationError(state),
  isActivated: selectActivationHasActivatedAccount(state)
});

const mapDispatchToProps = dispatch => ({
  getProfile: accessToken => dispatch(getProfile(accessToken)),
  activateAccount: (name, password, accessToken) =>
    dispatch(activateAccount(name, password, accessToken)),
  refreshAccessToken: () => dispatch(refreshAccessToken())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountActivationContainer);
