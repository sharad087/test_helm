import React from "react";
import ReactSVG from "react-svg";

import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import Popper from "@material-ui/core/Popper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import { ProfileIcon } from "../_shared";

const NavigationProfileButton = ({
  classes,
  profileImage,
  open,
  onToggleDropdown,
  onClose,
  anchorDropdownEl,
  onProfileClick,
  onLogoutClick,
  name,
  email
}) => {
  return (
    <div className="navigationProfileButton">
      <ClickAwayListener onClickAway={onClose}>
        <div>
          <div
            className="navigationProfileButton__profileIcon"
            onClick={onToggleDropdown}
          >
            <ProfileIcon name={name} src={profileImage} size="extraSmall" />
          </div>

          <Popper
            open={open}
            anchorEl={anchorDropdownEl}
            placement="bottom-end"
            disablePortal
            modifiers={{
              flip: {
                enabled: false
              },
              preventOverflow: {
                enabled: false,
                boundariesElement: "scrollParent"
              }
            }}
          >
            <div className="navigationProfileButton__dropdown">
              <MenuList
                disablePadding
                className="navigationProfileButton__dropdown__footer"
              >
                <div className="navigationProfileButton__dropdown__info">
                  <div className="navigationProfileButton__dropdown__profileImage">
                    <ProfileIcon
                      name={name}
                      src={profileImage}
                      size="extraSmall"
                    />
                  </div>

                  <div className="navigationProfileButton__dropdown__info__name">
                    <strong>{name}</strong>
                    <br />
                    {email}
                  </div>
                </div>

                <MenuItem onClick={onProfileClick}>
                  <ReactSVG src={require("../../images/my-profile.svg")} />
                  <div className="navigationProfileButton__action">
                    My Profile
                  </div>
                </MenuItem>
                <MenuItem onClick={onLogoutClick}>
                  <ReactSVG src={require("../../images/logout.svg")} />
                  <div className="navigationProfileButton__action">Log out</div>
                </MenuItem>
              </MenuList>
            </div>
          </Popper>
        </div>
      </ClickAwayListener>
    </div>
  );
};

NavigationProfileButton.defaultProps = {
  profileIcon: require("../../images/default_profile.svg"),
  name: ""
};

export default NavigationProfileButton;
