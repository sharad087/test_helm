import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withRouter } from "react-router-dom";
// import io from "socket.io";

import NavigationProfileButton from "./NavigationProfileButton";

import { logoutAccount } from "../../ducks/authenticate/actions";

import {
  selectAccountProfile,
  selectAccountIsLoadingProfile,
  selectAccountHasLoadedProfile
} from "../../ducks/account/selectors";

class NavigationProfileButtonContainer extends Component {
  constructor(props) {
    super(props);

    // this.socket = io(process.env.REACT_APP_API_URL, {
    //   path : 'INSERT PATH HERE'
    // })

    this.state = {
      anchorDropdownEl: null,
      openDropdown: false,
      profileImage: this.props.profile.profileImage || undefined
    };

    this.handleCloseDropdown = this.handleCloseDropdown.bind(this);
    this.handleToggleDropdown = this.handleToggleDropdown.bind(this);
    this.handleProfileClick = this.handleProfileClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }
  
  // componentDidMount(){
  //   this.socket.on("insert event name", () => console.log("do something"));
  // }

  componentDidUpdate(prevProps) {
    if (
      this.props.hasLoadedProfile &&
      this.props.hasLoadedProfile !== prevProps.hasLoadedProfile
    ) {
      const { profileImage } = this.props.profile;

      this.setState({
        profileImage: profileImage || undefined
      });
    }
  }

  handleCloseDropdown() {
    this.setState({ anchorDropdownEl: null, openDropdown: false });
  }

  handleToggleDropdown(event) {
    const { currentTarget } = event;
    this.setState(prevState => ({
      anchorDropdownEl: currentTarget,
      openDropdown: !prevState.openDropdown
    }));
  }

  handleProfileClick() {
    this.handleCloseDropdown();
    this.props.history.push("/dashboard/profile");
  }

  handleLogoutClick() {
    this.props.logoutAccount()
    // .then(this.props.history.push("/"))
  }

  render() {
    const { anchorDropdownEl, openDropdown } = this.state;
    const { name, email, profileImage } = this.props.profile;

    return (
      <NavigationProfileButton
        open={openDropdown}
        onToggleDropdown={this.handleToggleDropdown}
        onClose={this.handleCloseDropdown}
        anchorDropdownEl={anchorDropdownEl}
        onProfileClick={this.handleProfileClick}
        onLogoutClick={this.handleLogoutClick}
        name={name}
        email={email}
        profileImage={profileImage}
      />
    );
  }
}

const mapStateToProps = state => ({
  profile: selectAccountProfile(state),
  isProfileLoading: selectAccountIsLoadingProfile(state),
  hasLoadedProfile: selectAccountHasLoadedProfile(state)
});

const mapDispatchToProps = dispatch => {
  return {
    logoutAccount: () => dispatch(logoutAccount())
  };
};

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(NavigationProfileButtonContainer);
