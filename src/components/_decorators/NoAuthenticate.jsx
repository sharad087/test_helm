import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'

import { getDisplayName, readCookie } from "../../utils";
import {
  selectAuthenticateValid,
  selectAuthenticateError
} from "../../ducks/authenticate/selectors";

const NoAuthenticate = WrappedComponent => {
  class NoAuthenticateWrapper extends Component {
    constructor(props) {
      super(props);
      this.displayName = `NoAuthenticate(${getDisplayName(WrappedComponent)})`;
      console.log(this.displayName);
    }
    
    render() {
      const { valid } = this.props;
      const refreshToken = readCookie("refreshToken");
      
      if(valid || refreshToken){
        return <Redirect to="/dashboard"/>
      }else{
        return <WrappedComponent {...this.props} />;
      }
    }
  }

  const mapStateToProps = state => {
    return {
      error: selectAuthenticateError(state),
      valid: selectAuthenticateValid(state)
    };
  };

  return connect(
    mapStateToProps,
    null,
    )(NoAuthenticateWrapper);
};

export default NoAuthenticate;
