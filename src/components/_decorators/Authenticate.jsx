import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { getDisplayName, readCookie, eraseCookie } from "../../utils";
import { connect } from "react-redux";

import { refreshAccessToken } from "../../ducks/authenticate/actions";
import {
  selectAuthenticateBusy,
  selectAuthenticateValid,
  selectAuthenticateError
} from "../../ducks/authenticate/selectors";

const Authenticate = WrappedComponent => {
  class AuthenticateWrapper extends Component {
    constructor(props) {
      super(props);
      console.log("AuthenticateWrapper");
      this.displayName = `Authenticate(${getDisplayName(WrappedComponent)})`;

      this.state = {
        checkedRefreshToken: false,
      }
    }

    componentDidMount() {
      // Check if user is logged in
      if (readCookie("refreshToken") || this.props.valid) {
        this.props.refreshAccessToken().then(() => {
          this.setState({checkedRefreshToken: true});

          //If refreshAccessToken return an error
          if (this.props.error) {
            eraseCookie("refreshToken");
            eraseCookie("accessToken");
          }
        });

        try {
          // Retrieve a new Access Token every 60 minutes
          setInterval(() => {
            this.props.refreshAccessToken();
            console.log("Auto retrieved new access Token");
          }, 3600000);
        } catch (err) {
          console.log("Failed to auto retrieve", err);
        }
      } else {
        console.log("no refresh token or invalid");
        this.setState({checkedRefreshToken: true});
      }
    }

    render() {
      const { checkedRefreshToken } = this.state;

      if (!this.props.busy && this.props.valid && !this.props.error && checkedRefreshToken) {
        //Authorized
        return <WrappedComponent {...this.props} />;
      } else if (!this.props.busy && !this.props.valid && checkedRefreshToken) {
        //Invalid/Not Authenticated Redirect
        return <Redirect to="/" />;
      } else {
        return <div>Loading</div>;
      }
    }
  }

  const mapPropsToState = state => {
    return {
      busy: selectAuthenticateBusy(state),
      valid: selectAuthenticateValid(state),
      error: selectAuthenticateError(state)
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      refreshAccessToken: () => dispatch(refreshAccessToken())
    };
  };

  return connect(
    mapPropsToState,
    mapDispatchToProps
  )(AuthenticateWrapper);
};

export default Authenticate;
