import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";

const OrganizationVerification = WrappedComponent => {
  class OrganizationVerificationWrapper extends Component {
    constructor(props) {
      super(props);
      this.url = 'test.immerse.online'

      this.state = {
        valid: false,
        busy: true,
        error: false,
      };
    }

    componentDidMount() {
      let subdomain = "";
      const urlParts = this.url.split('.');
      if (urlParts.length === 3) {
        subdomain = urlParts[0];
      }

      this.setState({ busy: true });

      if(subdomain) {
        axios.get(`/public/organizations`, {
          params: {
            url: subdomain
          }
        })
        .then(() => {
          this.setState({busy: false, error: false, valid: true});
        },
        () => {
          this.setState({busy: false, error: true, valid: false});
        });
      }else {
        this.setState({busy: false, error: true, valid: false});
      }
    }

    render() {
      const { valid, busy, error } = this.state;

      if (valid && !busy && !error) {
        return <WrappedComponent {...this.props} />;
      } else if (!valid && !busy && error) {
        return <Redirect to="/redirect" />;
      } else {
        return <div></div>;
      }
    }
  }

  return OrganizationVerificationWrapper;
};

export default OrganizationVerification;