import React from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';

const NavigationButton = (props) => {
  const buttonClassName = props.selected ? 'navigation-button--selected' : 'navigation-button'
  return (
    <div onClick={props.onClick} className={buttonClassName}>
      <div className="navigation-button__iconContainer">
        <ReactSVG src={props.icon} svgClassName={props.selected ? "navigation-button__icon--selected" : "navigation-button__icon"}/>
      </div>
      <div>{props.text}</div>
    </div>
  );
};

NavigationButton.defaultProps = {
  selected: false,
  text: ''
};

NavigationButton.propTypes = {
  selected: PropTypes.bool,
  text: PropTypes.string
};

export default NavigationButton;
