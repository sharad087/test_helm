import React, { Component } from 'react';
import NavigationButton from './NavigationButton';
import PropTypes from 'prop-types';

export default class NavigationButtonContainer extends Component {
  render () {
    return (
      <NavigationButton 
      selected={this.props.selected} 
      onClick={this.props.onClick} 
      icon={this.props.icon}
      text={this.props.text}>
        {this.props.children}
      </NavigationButton>
    );
  }
}

NavigationButtonContainer.defaultProps = {
  selected: false,
  text: ''
};

NavigationButtonContainer.propTypes = {
  selected: PropTypes.bool,
  text: PropTypes.string
};
