export default {
  "CAD": "CA$",
  "CHF": "CHF",
  "CNY": "CN¥",
  "EUR": "€",
  "GBP": "£",
  "JPY": "¥",
  "KRW": "₩",
  "USD": "$"
}