import axios from "axios";

export default function configureAxios(){
  axios.defaults.baseURL = process.env.REACT_APP_API_URL || "";
  axios.defaults.headers.common = {
    "Api-Key": process.env.REACT_APP_API_KEY || "",
    "Api-Secret": process.env.REACT_APP_API_SECRET || ""
  };
} 