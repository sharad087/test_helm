FROM node:10 as student-app

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

ENV PORT='3003'
ENV REACT_APP_API_URL="https://api.immersehq.online"
ENV REACT_APP_API_KEY="ff08d016a4cca5ec50faef6db87e91fa96d963668df7017ac913f3059a88a46e"
ENV REACT_APP_API_SECRET="d8913b4247e2b16e4c3dbbd743596a32b75e3c13356f1b4eb263933e2d793e50fe56adb026c8384f7d135bf89cb5e6c7b12c85e6a37ca42dfdc56020c15d02a52607b97ac279a38da4c72d4756eee1de1b31e96d86ab16308e4ca480ecc00381b60820e78728082818cbc04ea73596968164bccdd407a2086e41ac60f939a26c9e59dfbf4ba3a4d703485309710457c5287c7c53ddddd0a4989cd6b2b5056a175436980595c03b187f05f6a83b089205f0c11a751fc4714de349b4e24064351736dd4037ead45301c6734f51f406760cc5b7e4c32ca0c6fdec45b7a7962f6ff63b99ec0fa7013c06c6554032bf658a5e8512e592db14137c4f213cf6b3125fcd"
ENV REACT_APP_IMMERSE_LOBBY_URL="https://learn.immersehq.online"

RUN npm run build

RUN npm install serve -g

CMD serve -s build

# FROM nginx:1.12-alpine

# COPY --from=admin-app /usr/src/app/build /usr/share/nginx/html

# EXPOSE 80

# CMD ["nginx", "-g", "daemon off;"]

